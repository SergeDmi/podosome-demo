import src.analyze_article as aa
import src.plot_results as pc
import numpy as np
import pandas as pd

if __name__=="__main__":

    analyzed = aa.article_analyze(load_config="config_loader.yaml", analysis_config="config_analysis.yaml",
                                  characters="data_characteristics.csv")
    radii = analyzed.podosomes[0]["radial_results"]["radii"]

    ## Saving
    # Saving radial results
    for podosome in analyzed.podosomes:
        df = pd.DataFrame.from_dict(podosome["radial_results"])
        df.to_csv("results/individual/radial_results_%s.csv" %podosome["podosome_name"])
    # Saving inner vs outer
    core = pd.DataFrame.from_records([podosome["core_results"] for podosome in analyzed.podosomes])
    outr = pd.DataFrame.from_records([podosome["outr_results"] for podosome in analyzed.podosomes])
    core.to_csv("results/general/core.csv")
    outr.to_csv("results/general/outr.csv")

    stats_in = analyzed.analysis["stats_in"]
    stats_out = analyzed.analysis["stats_out"]
    print(f"Mean fil length in : {stats_in[0]} +/- {stats_in[1]}")
    print(f"Mean fil length out : {stats_out[0]} +/- {stats_out[1]}")

    # Plotting orientation
    orientations = [ podosome["radial_results"]["AVG_orientation"] for podosome in analyzed.podosomes]
    numbers = [podosome["outr_results"]["number"] for podosome in analyzed.podosomes]
    fits_orient  = [ podosome["fit_orient"] for podosome in analyzed.podosomes]
    pc.plot_data_as_function_of_radius(radius_centered = radii, fits = fits_orient, results = orientations,
                                       filename = "results/plots/orientation.pdf", tag = "orientation")
    radiuses = [f["radius_core"] for f in fits_orient]
    print(f"Mean core radius : {np.mean(np.array(radiuses))} +/- {np.std(np.array(radiuses))}")
    pc.plot_core_radius(radiuses, "results/plots/insert_core_radius.pdf")

    number_mb = [podosome["core_results"]["number_mb"] for podosome in analyzed.podosomes]
    av_angle_mb = [podosome["core_results"]["AVG_angle_mb"] for podosome in analyzed.podosomes]
    st_orient_mb = [podosome["core_results"]["STD_angle_mb"] for podosome in analyzed.podosomes]
    pc.plot_protrusive_filaments(orientation=av_angle_mb, orientation_std=st_orient_mb,
                                 number_mb=number_mb, filename="results/plots/orient_number_mb.pdf")
    print(f"Mean number mb : {np.mean(number_mb)} ; std : {np.std(number_mb)}")
    print(f"Mean angle mb : {np.mean(av_angle_mb)} ; std : {np.std(av_angle_mb)}")

    # Forces
    forces_mb = [podosome["core_results"]["force_mb"]/1000 for podosome in analyzed.podosomes]
    forces_el = [podosome["core_results"]["elastic_force"] for podosome in analyzed.podosomes]
    pc.plot_forces(forces_mb = forces_mb, forces_el=forces_el, filename = "results/plots/forces_comp.pdf")
    pc.plot_forces_protrusive(forces_mb=forces_mb, filename="results/plots/forces_protrusive.pdf")
    print(f"Mean elastic force : {np.mean(forces_el)} ; std : {np.std(forces_el)}")
    print(f"--------------- min : {np.min(forces_el)} ; max : {np.max(forces_el)}")
    print(f"Mean protrusive force : {np.mean(forces_mb)} ; std : {np.std(forces_mb)}")


    # Pressure
    pressures_el = [podosome["core_results"]["pressure_el"]/1000 for podosome in analyzed.podosomes]
    pressures_mb = [podosome["core_results"]["pressure_mb"]/1000000 for podosome in analyzed.podosomes]
    pc.plot_pressure(pressure_mb=pressures_mb, pressure_el=pressures_el, filename="results/plots/pressure_comp.pdf")
    print(f"Mean elastic pressure : {np.mean(pressures_el)} ; std : {np.std(pressures_el)}")

    # Plotting correlation length
    corr_in = [podosome["core_results"]["corr_length"]/1000 for podosome in analyzed.podosomes]
    corr_out = [podosome["outr_results"]["corr_length"]/1000 for podosome in analyzed.podosomes]
    pc.plot_corr_in_out(values_in=corr_in, values_out=corr_out,
                        filename="results/plots/correlation_inout.pdf",xlabel=None,ylabel="Correlation length ($\mu m$)")
    pc.plot_in_out(values_in=corr_in, values_out=corr_out, filename="results/plots/correlation_bis.pdf", xlabel=None,
                        ylabel="Correlation length ($\mu m$)",ymin=0,ymax=3.5)
    print(f"Mean correlation length core : {np.mean(corr_in)} ; outside : {np.mean(corr_out)}")


