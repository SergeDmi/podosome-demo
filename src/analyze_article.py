import src.podosome_analyzer as poda
import numpy as np
import yaml
from scipy.optimize import curve_fit

__KT_TO_JOULES__ = 4e-21
__NM_TO_UM__ = 10.0**(-3.0)
__DEFAULT_FA__ = 10.0

""" This is a set of scripts for the analysis of podosomes 
        the function to be called is article_analyze(...)
"""


def article_analyze(*args, load_config=None, analysis_config=None, characters=None, **kwargs):
    """
        Analyzes the podosome, extracting all information required for our article
        This does not perform the plotting nor the saving, just the analysis
        Thus this function is the one that should be called for scripts or notebooks

        results are stored in loaded.podosomes as dictionaries

    """
    analysis = {}
    with open(analysis_config, 'r') as stream:
        try:
            config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    try:
        loaded = poda.Analyzer(*args, config=load_config, characters=characters, **kwargs)
    except:
        ValueError("Did not manage to load podosomes !")

    # Radius_lst is a list of radii, used as bins for radial analysis
    radius_lst = list(np.linspace(config['radius']['min'], config['radius']['max'], config['radius']['number_points']))
    # Pre-analysis performed in the Analyzer class in podosome_analyzer
    loaded.perform_analysis(analysis=analysis)
    # Doing individual analysis for each podosome
    for podosome in loaded.podosomes:
        # Radial analysis first
        results = compute_radial_properties(podosome, *args, analyzer=loaded,
                                            radius_lst=radius_lst, correlations=False, **kwargs)
        podosome["radial_results"] = results
        get_radial_fits(podosome, radius_lst)

        # Then analyzing core vs radial
        core_radius = podosome["fit_orient"]["radius_core"]
        rad_lst = [0.0, core_radius, np.inf]
        results_core_out = compute_radial_properties(podosome, *args, analyzer=loaded,
                                                     radius_lst=rad_lst, correlations=True, config=config, **kwargs)
        # Splitting that in two separate dictionaries
        expand_core_outr_results(podosome, results_core_out, core_radius, config=config, *args, **kwargs)

    #loaded.analysis["stats_in"], loaded.analysis["stats_out"] = stats_filaments(loaded.podosomes)
    return loaded

def compute_radial_properties(podosome, *args, analyzer=None, correlations = None, radius_lst=None, **kwargs):
    """ Here we compute the properties in radial donuts
        there can be many donuts (typical radial analysis)
        or 2 donuts (core vs outer)
    """
    results = {}
    segments = podosome["segments"]
    ns = len(radius_lst) - 1
    zmin = podosome.get("zmin", None)
    zmax = podosome.get("zmax", None)

    # Initialization
    keys = ["tot_length", "fil_length", "length_density", "AVG_orientation", "STD_orientation",
            "energy", "energy_density", "radii", "strain", "corr_length", "energy_per_length", "number"]

    for key in keys:
        results[key] = []

    for i, rmin in enumerate(radius_lst[:-1]):
        rmax = radius_lst[i + 1]
        results['radii'].append((rmin+rmax)/2.0)
        volume = (rmax*rmax - rmin*rmin) * np.pi * podosome["core_height"] * __NM_TO_UM__**3.0 # in µm3

        # Segments
        segs = segments[poda.in_radius(segments, rmin, rmax, zmin=zmin, zmax=zmax), :]
        results['tot_length'].append(np.sum(poda.array_norm(segs[:, 4:7])) * __NM_TO_UM__)
        results['length_density'].append(results['tot_length'][i]/volume)
        orientations = poda.get_orientations(segs)
        results['AVG_orientation'].append(np.mean(orientations))
        results['STD_orientation'].append(np.std(orientations))

        # Slow : computing correlation functions
        if correlations is not None:
            cors = poda.get_correlations(segs)
            half = np.max(np.argwhere(cors[:, 2] > np.max(cors[:, 2]) / 2.0)) + 1
            explen = fit_exp(cors[0:half, 0], cors[0:half, 1], bounds=([0], [20000]), p0=[2000])
            results['corr_length'].append(explen)
        else:
            results['corr_length'].append(None)


        # Points
        energy, lengths, counts, strains, unwed_strains = 0.0, 0.0, 0.0, 0.0, 0.0

        count_in = 0
        for filament in podosome["filaments"]:
            pts_inside = poda.in_radius(filament, rmin, rmax)
            fils = filament[pts_inside, :]
            count = np.shape(fils)[0]
            energy  += np.sum(fils[:, 5])
            ll = poda.fil_length(filament)
            lengths += count*ll
            counts  += count
            strains += count * (1.0 - np.linalg.norm(filament[-1, 1:4]-filament[0, 1:4]) / ll)
            if count > 0:
                count_in += 1

        results['fil_length'].append(lengths/counts)
        results['energy'].append(energy)
        results['energy_per_length'].append( energy / (results['tot_length'][i] * __KT_TO_JOULES__))
        results['energy_density'].append(energy/(volume * __KT_TO_JOULES__) )
        results['strain'].append(strains / counts)
        results['number'].append(count_in)

    return results

def stats_filaments(podosomes):
    """ Returns statistics of filament length inside and outside the core
    Inside is defined as not having points outside"""
    n_in = np.sum([ podosome["core_results"]["number"] for podosome in podosomes])
    fil_in = np.zeros((n_in, ))
    n_out = np.sum([podosome["outr_results"]["number"] for podosome in podosomes])
    fil_out = np.zeros((n_out,))
    nI, nO = 0, 0
    for podosome in podosomes:
        core_radius = podosome["fit_orient"]["radius_core"]
        for filament in podosome["filaments"]:
            if np.sum(poda.in_radius(filament, core_radius, np.inf)):
                fil_out[nO] = poda.fil_length(filament)
                nO += 1
            else:
                fil_in[nI] = poda.fil_length(filament)
                nI += 1
    fil_out = fil_out[0:nO]
    fil_in = fil_in[0:nI]
    return [np.mean(fil_in), np.std(fil_in)], [np.mean(fil_out), np.std(fil_out)]

def analyze_touching(segments, *args, polymerization_force=None, core_radius = None, config=None, **kwargs):
    """ Analyzes segments touching the membrane"""
    if core_radius is None:
        core_radius = np.inf

    segs = segments[poda.in_radius(segments, 0, core_radius), :]
    results = {}
    if polymerization_force is not None:
        fa = polymerization_force
    else:
        try:
            fa = config["actin"]["polymerization_force"]
        except:
            fa = __DEFAULT_FA__
            print("Warning : polymerization force set to default : %s pN" % fa)
    touching = poda.segments_touching(segs)


    if touching is not None:
        ixes = np.unique(touching[:, 0])
        results["number_mb"] = len(ixes)
        angles = np.array([np.mean(poda.get_orientations(touching[touching[:, 0] == ix, :])) for ix in ixes])
        results["AVG_angle_mb"] = np.mean(angles)
        results["STD_angle_mb"] = np.std(angles)
        results["force_mb"] = fa * np.sum(1.0 / np.sin((angles * np.pi)/180))
    else:
        print("WARNING : no filaments touching the membrane, this is suspicious")
        results["number_mb"] = 0
        results["AVG_angle_mb"] = float("NaN")
        results["STD_angle_mb"] = float("NaN")
        results["force_mb"] = 0
    return results

def get_radial_fits(podosome, radius_lst):
    """ Fits from the results of the radial analysis """
    results = podosome["radial_results"]
    # Fitting the results
    podosome["fit_orient"] = poda.fit_to_step(results["radii"], results["AVG_orientation"])
    bounds = ([min(radius_lst), 0, 0, 0], [max(radius_lst), 1000, 1000, 1000])
    podosome["fit_length"] = poda.fit_to_step(results["radii"], results["fil_length"], bounds=bounds)
    podosome["fit_density"] = poda.fit_to_step(results["radii"], results["length_density"])
    bounds = ([min(radius_lst), 0, 0, 0], [max(radius_lst), 2e6, 2e6, 1e3])
    podosome["fit_energy"] = poda.fit_to_step(results["radii"], results["energy_density"], bounds=bounds)
    pass

def expand_core_outr_results(podosome, results_core_out, core_radius, *args, **kwargs):
    """  Saves the results from the core and periphery separately
        also computes the polymerization force and pressure """
    outr_results = {}
    core_results = analyze_touching(podosome["segments"], core_radius = core_radius, **kwargs)
    for key in results_core_out:
        core_results[key] = results_core_out[key][0]
        outr_results[key] = results_core_out[key][1]

    core_results["elastic_force"] = core_results["energy"] / \
                                    (core_results["strain"] * podosome["core_height"] * (__NM_TO_UM__ ** 6.0))
    core_results["pressure_el"] = core_results["elastic_force"] / (
            (__NM_TO_UM__ ** 3.0) * np.pi * core_radius ** 2.0)
    core_results["pressure_mb"] = core_results["force_mb"] / (
            (__NM_TO_UM__ ** 3.0) * np.pi * core_radius ** 2.0)
    core_results["radius"] = core_radius
    podosome["core_results"] = core_results
    podosome["outr_results"] = outr_results
    pass

def fit_exp(X,Y,**kwargs):
    """ Fits to a decreasing exponential funtion"""
    def exp_neg(x,l):
        return np.exp(-x/l)
    params = curve_fit(exp_neg, X, Y, **kwargs)
    return params[0][0]

