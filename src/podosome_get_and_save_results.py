# generic import
import math
import os
import numpy as np
import csv
import re
import yaml
import pandas as pd
#import modules
import src.podosome_analysis as poda

################################################################################################################################################################################
#### CONFIG PARAMETERS ###############################################################################################################################################################
################################################################################################################################################################################

config_yaml = open('config.yaml')
config_dict = yaml.load(config_yaml, Loader=yaml.FullLoader)
####################################
##Actin parameters #################
__RIGIDITY_ACTIN_FILAMENT__ = float(config_dict['Actin']['rigidity'])
__ACTIN_MONOMER_SIZE__ = float(config_dict['Actin']['monomer_size'])
####################################
##Values from AFP experiments ######
__FORCE_EXP_AFM__ = (float(config_dict['AFM_experiment']['force']), float(config_dict['AFM_experiment']['std_force'])) #tuple (value,standard error)
__CORE_RADIUS_EXP_AFM__ = (float(config_dict['AFM_experiment']['core_radius']), float(config_dict['AFM_experiment']['std_core_radius'])) #tuple (value,standard error)
####################################
##Thermal energy ###################
__KT_TO_JOULES__= float(config_dict['KT_TO_JOULES'])
####################################
##Conversion factors ###############
___STD_TO_PICO___ = float(config_dict['Unit_conversion']['STD_TO_PICO'])
___STD_TO_NANO___ = float(config_dict['Unit_conversion']['STD_TO_NANO'])
___PICO_TO_NANO__ = float(config_dict['Unit_conversion']['PICO_TO_NANO'])
___PICO_TO_STD__ = float(config_dict['Unit_conversion']['PICO_TO_STD'])
___NANO_TO_STD__ = float(config_dict['Unit_conversion']['NANO_TO_STD'])

################################################################################################################################################################################
#### GET RESULTS ###############################################################################################################################################################
################################################################################################################################################################################

def compute(lst = None, config = None, podosome_characteristics = None, **kwargs):
    dict = {}
    for string in lst:
        dict[string] = get_results_all(tag = string, config = config, podosome_characteristics = podosome_characteristics)
    return dict

def get_results_all(tag = None, config = None, podosome_characteristics = None, **kwargs):
    if tag == 'length':
        dict = get_results_filament_length(podosome_characteristics = podosome_characteristics, config = config)
    elif tag == 'length_height':
        dict = get_results_filament_length_height(config = config, podosome_characteristics = podosome_characteristics)
    elif tag == 'curvature':
        dict = get_results_filament_curvature(config = config, podosome_characteristics = podosome_characteristics)
    elif tag == 'compression':
        dict = get_results_filament_compression(config = config, podosome_characteristics = podosome_characteristics)
    elif tag == 'orientation':
        dict = get_results_filament_orientation(podosome_characteristics = podosome_characteristics, config = config)
    elif tag == 'filament_density':
        dict = get_results_filament_density(podosome_characteristics = podosome_characteristics, config = config)
    elif tag == 'density_energy':
        dict = get_results_density_energy(podosome_characteristics = podosome_characteristics, config = config)
    elif tag == 'elastic_force':
        dict = get_results_elastic_force(podosome_characteristics = podosome_characteristics)
    elif tag == 'polymerization_force':
        dict = get_results_polymerization_force(podosome_characteristics = podosome_characteristics)
    elif tag == 'filament_orientation_core':
        dict = get_results_distribution_orientation_core(podosome_characteristics = podosome_characteristics)
    elif tag == 'filament_orientation_outside':
        dict = get_results_distribution_orientation_outside(podosome_characteristics = podosome_characteristics)
    elif tag == 'filament_length_core':
        dict = get_results_distribution_length_core(podosome_characteristics = podosome_characteristics)
    elif tag == 'filament_length_outside':
        dict = get_results_distribution_length_outside(podosome_characteristics = podosome_characteristics)
    return dict

##############################
##### Radial distance analysis
##############################

######################################################
## Filament orientation ##############################
def get_results_filament_orientation(config = None, podosome_characteristics = None, **kwargs):
    data_podosome = pd.read_csv(podosome_characteristics) #Read podosome_characteristics
    filename_dict = get_filename_dict(data_podosome['filename']) #Creates a dictionnary containing all input filenames for analysis
    Radius_lst = list(np.linspace(config['Radius']['min'],config['Radius']['max'],config['Radius']['number_points']))
    delta_radius = Radius_lst[1]-Radius_lst[0] #Get radial distance interval
    Results_filament_orientation = {}
    for key in filename_dict:
        Results_filament_orientation[key] = get_AVG_filament_orientation_radius(filename = filename_dict[key], radius = Radius_lst, interval =  delta_radius)  #Filament orientation as a function of radius (for all heights)
    return Results_filament_orientation

def get_AVG_filament_orientation_radius(filename = None, **kwargs):
    data = poda.get_data(filename)
    filament_number = poda.get_filament_number(data)
    point_number_filament = poda.get_point_number_filament(data)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    orientation_all_filaments, height_all_filaments, radial_distance_all_filaments = poda.get_orientation_all_filaments(x_position, y_position, z_position, filament_number, point_number_filament, filename)
    orientation_radius_lst, AVG_orientation, STD_orientation, SEM_orientation, len_interval = poda.get_list_average_filament_orientation(orientation_all_filaments,height_all_filaments,radial_distance_all_filaments, kwargs['radius'], kwargs['interval'])
    return {'AVG_orientation':AVG_orientation,'STD_orientation':STD_orientation,'SEM_orientation':SEM_orientation,'Radius_lst': kwargs['radius'],'len_interval':len_interval}

######################################################
## Filament curvature ################################
def get_results_filament_curvature(config = None, podosome_characteristics = None, **kwargs):
    data_podosome = pd.read_csv(podosome_characteristics) #Read Input file
    filename_dict = get_filename_dict(data_podosome['filename']) #Creates a dictionnary containing all input filenames
    Radius_lst = list(np.linspace(config['Radius']['min'],config['Radius']['max'],config['Radius']['number_points']))
    delta_radius = Radius_lst[1]-Radius_lst[0] #Get radial distance interval
    Results_filament_curvature = {}
    for key in filename_dict:
        Results_filament_curvature[key] = get_AVG_filament_local_curvature_radius(filename = filename_dict[key], radius = Radius_lst, interval = delta_radius) #Filament length (using weighted method) as a function of radius
    return Results_filament_curvature

def get_AVG_filament_local_curvature_radius(filename = None, **kwargs):
    data = poda.get_data(filename)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    point_number_filament = poda.get_point_number_filament(data)
    filament_number = poda.get_filament_number(data)
    filament_curvature = poda.get_mean_local_curvature(data)
    x_core, y_core = poda.get_core_coordinates(filename)
    radial_distance = poda.get_radial_distance_all_filament(x_position, y_position, x_core, y_core)
    AVG_curvature, STD_curvature, SEM_curvature, len_interval = poda.get_average_curvature_method_weighted(filament_number, filament_curvature, radial_distance, kwargs['radius'], kwargs['interval'])
    return {'AVG_curvature':AVG_curvature,'STD_curvature':STD_curvature,'SEM_length':SEM_curvature,'Radius_lst':kwargs['radius'],'len_interval':len_interval}

######################################################
## Filament compression ##############################
def get_results_filament_compression(config = None, podosome_characteristics = None, **kwargs):
    data_podosome = pd.read_csv(podosome_characteristics) #Read Input file
    filename_dict = get_filename_dict(data_podosome['filename']) #Creates a dictionnary containing all input filenames
    Radius_lst = list(np.linspace(config['Radius']['min'],config['Radius']['max'],config['Radius']['number_points']))
    delta_radius = Radius_lst[1]-Radius_lst[0] #Get radial distance interval
    Results_filament_compression = {}
    for key in filename_dict:
        Results_filament_compression[key] = get_AVG_filament_compression_radius(filename = filename_dict[key], radius = Radius_lst, interval = delta_radius) #Filament length (using weighted method) as a function of radius
    return Results_filament_compression

def get_AVG_filament_compression_radius(filename = None, radius = None, interval = None, **kwargs):
    data = poda.get_data(filename)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    point_number_filament = poda.get_point_number_filament(data)
    filament_number = poda.get_filament_number(data)
    filament_length = poda.get_filament_length(data)
    x_core, y_core = poda.get_core_coordinates(filename)
    radial_distance = poda.get_radial_distance_all_filament(x_position, y_position, x_core, y_core)
    AVG_compression,STD_compression,SEM_compression,len_interval,avg_lst_length,std_lst_length,sem_lst_length,len_interval_lst_length,avg_lst_curvature_predict,std_lst_curvature_predict,sem_lst_curvature_predict,len_interval_lst_curvature_predict=poda.get_average_compression_method_weighted(x_position, y_position, z_position, filament_number, filament_length, radial_distance, radius, interval)
    return {'AVG_compression':AVG_compression,'STD_compression':STD_compression,'SEM_compression':SEM_compression,'Radius_lst':radius,'len_interval':len_interval}

######################################################
## Filament length ###################################
def get_results_filament_length(config = None, podosome_characteristics = None, **kwargs):
    data_podosome = pd.read_csv(podosome_characteristics) #Read Input file
    filename_dict = get_filename_dict(data_podosome['filename']) #Creates a dictionnary containing all input filenames
    Radius_lst = list(np.linspace(config['Radius']['min'],config['Radius']['max'],config['Radius']['number_points']))
    delta_radius = Radius_lst[1]-Radius_lst[0] #Get radial distance interval
    Results_filament_length = {}
    for key in filename_dict:
        Results_filament_length[key] = get_AVG_filament_length_radius(filename = filename_dict[key], radius = Radius_lst, interval = delta_radius) #Filament length (using weighted method) as a function of radius
    return Results_filament_length

def get_AVG_filament_length_radius(filename = None, **kwargs):
    data = poda.get_data(filename)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    point_number_filament = poda.get_point_number_filament(data)
    filament_number = poda.get_filament_number(data)
    filament_length = poda.get_filament_length(data)
    x_core, y_core = poda.get_core_coordinates(filename)
    radial_distance = poda.get_radial_distance_all_filament(x_position, y_position, x_core, y_core)
    AVG_length, STD_length, SEM_length, len_interval = poda.get_average_filament_length(filament_number, point_number_filament, filament_length, radial_distance, kwargs['radius'], kwargs['interval'])
    return {'AVG_length':AVG_length,'STD_length':STD_length,'SEM_length':SEM_length,'Radius_lst':kwargs['radius'],'len_interval':len_interval}

######################################################
## Filament density ##################################
def get_results_filament_density(config = None, podosome_characteristics = None, **kwargs):
    data_podosome = pd.read_csv(podosome_characteristics)
    filename_dict = get_filename_dict(data_podosome['filename'])
    Radius_lst = list(np.linspace(config['Radius']['min'],config['Radius']['max'],config['Radius']['number_points']))
    delta_radius = Radius_lst[1]-Radius_lst[0] #Get radial distance interval
    height_core_lst = data_podosome['core_height']
    Results_filament_density = {}
    index = 0
    for key in filename_dict:
        Results_filament_density[key] = get_density_filament_radius(filename = filename_dict[key], radius = Radius_lst, interval = delta_radius, height_core_value = height_core_lst[index])
        index = index + 1
    return Results_filament_density

def get_density_filament_radius(filename = None, interval = None, radius = None, height_core_value = None, **kwargs):
    data = poda.get_data(filename)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    point_number_filament = poda.get_point_number_filament(data)
    filament_number = poda.get_filament_number(data)
    filament_length = poda.get_filament_length(data)
    x_core, y_core = poda.get_core_coordinates(filename)
    radial_distance = poda.get_radial_distance_all_filament(x_position, y_position, x_core, y_core)
    density_radius = poda.get_density_radius(filament_number, point_number_filament, filament_length, radial_distance, interval, height_core_value, radius)
    return {'density':density_radius,'Radius_lst':radius}

########################################################
## Density of energy ###################################
def get_results_density_energy(config = None, podosome_characteristics = None, **kwargs):
    data_podosome = pd.read_csv(podosome_characteristics)
    filename_dict = get_filename_dict(data_podosome['filename'])
    Radius_lst = list(np.linspace(config['Radius']['min'],config['Radius']['max'],config['Radius']['number_points']))
    delta_radius = Radius_lst[1]-Radius_lst[0] #Get radial distance interval
    height_core_lst = data_podosome['core_height']
    index = 0
    Results_density_energy = {}
    for key in filename_dict:
        Results_density_energy[key] = get_density_energy_radius(filename = filename_dict[key], radius = Radius_lst, interval = delta_radius, height_core_value = height_core_lst[index])
        index = index + 1
    return Results_density_energy

def get_density_energy_radius(filename = None, interval = None, radius = None, height_core_value = None, **kwargs):
    data = poda.get_data(filename)
    local_energy = poda.get_loc_energy(data)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    filament_number = poda.get_filament_number(data)
    filament_length = poda.get_filament_length(data)
    point_number_filament = poda.get_point_number_filament(data)
    x_core, y_core = poda.get_core_coordinates(filename)
    radial_distance = poda.get_radial_distance_all_filament(x_position, y_position, x_core, y_core)
    density_elastic_energy_volume = poda.get_density_elastic_energy(filament_number,local_energy,radial_distance,radius,interval,height_core_value)
    return {'density_elastic_energy_volume':density_elastic_energy_volume,'Radius_lst':radius}

###############################################
##### Further analysis of filament organization
###############################################

########################################################
## Length as a function of the height ##################
def get_results_filament_length_height(config = None, podosome_characteristics = None, **kwargs):
    data_podosome = pd.read_csv(podosome_characteristics) #Read Input file
    filename_dict = get_filename_dict(data_podosome['filename']) #Creates a dictionnary containing all input filenames
    radius_core_lst =  data_podosome['radius_core']
    shift_value_lst = data_podosome['shift']
    tomo_tag_lst = data_podosome['tomo']
    Height_list = list(np.linspace(0,300,13))
    Results_filament_length = {}
    index = 0
    for key in filename_dict:
        Results_filament_length[key] = get_AVG_filament_length_height(filename = filename_dict[key], shift_value = shift_value_lst[index], tomo_tag = tomo_tag_lst[index], radius_core_value = radius_core_lst[index], Height_list = Height_list) #Filament length as a function of height (z-axis)
        index = index + 1
    return Results_filament_length

def get_AVG_filament_length_height(filename = None, shift_value = None, tomo_tag = None, radius_core_value = None, Height_list = None, **kwargs):
    data = poda.get_data(filename)
    filament_number = poda.get_filament_number(data)
    point_number_filament = poda.get_point_number_filament(data)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    z_position_corrected = poda.correct_z_position(z_position = z_position, tag = tomo_tag, shift = shift_value)
    filament_length = poda.get_filament_length(data)
    filament_length_all, radial_distance_all, filament_height_all = poda.get_length_all_filaments_height(x_position, y_position, z_position_corrected, filament_number, point_number_filament, filament_length, filename)
    filament_length_core, filament_height_core = poda.get_length_all_filaments_height_radial_selection(radial_distance_all, filament_length_all, filament_height_all,0, radius_core_value)
    delta_height = Height_list[1]-Height_list[0] #Get binning interval
    avg_length_lst, std_length_lst = poda.get_list_average_filament_length_height_corrected(filament_length_core, filament_height_core, Height_list, delta_height)
    return {'AVG_length':avg_length_lst,'STD_length':std_length_lst,'AVG_height':Height_list,'AVG_height_percent':Height_list}

###########################################################
## Distribution of filament orientation inside the core ###
def get_results_distribution_orientation_core(podosome_characteristics = None, **kwargs):
    ## Get dictionnary of filenames
    data_podosome = pd.read_csv(podosome_characteristics) ## Read Input file
    filename_dict = get_filename_dict(data_podosome['filename']) #Creates a dictionnary containing all input filenames
    ## Read fit parameters
    data_fit_orientation = pd.read_csv('./results/general/orientation_fit_parameters.csv')
    radius_core_lst =  data_fit_orientation['radius_core']
    radius_scale_lst =  data_fit_orientation['parameter_c']
    Results = {}
    index = 0
    for key in filename_dict:
        Results[key] = get_distribution_orientation_core(filename = filename_dict[key], radius_core_value = radius_core_lst[index], radius_scale_value = radius_scale_lst[index])
        index = index + 1
    return Results

def get_distribution_orientation_core(filename = None, radius_core_value = None, radius_scale_value = None, **kwargs):
    ##########################
    ## Extract data ##########
    data = poda.get_data(filename)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    filament_number = poda.get_filament_number(data)
    filament_length = poda.get_filament_length(data)
    point_number_filament = poda.get_point_number_filament(data)
    elastic_energy = poda.get_local_energy(data)#sum of local energy
    local_energy = poda.get_loc_energy(data)#local energy
    ## Get dictionnary containing lists of filament orientation inside the podosome core
    dict = poda.get_filament_orientation_core(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filament_length = filament_length, local_energy = local_energy, filename = filename, core_radius = radius_core_value, radius_scale = radius_scale_value)
    return dict

############################################################
## Distribution of filament orientation outside the core ###
def get_results_distribution_orientation_outside(podosome_characteristics = None, **kwargs):
    ## Get dictionnary of filenames
    data_podosome = pd.read_csv(podosome_characteristics) ## Read Input file
    filename_dict = get_filename_dict(data_podosome['filename']) #Creates a dictionnary containing all input filenames
    ## Read fit parameters
    data_fit_orientation = pd.read_csv('./results/general/orientation_fit_parameters.csv')
    radius_core_lst =  data_fit_orientation['radius_core']
    radius_scale_lst =  data_fit_orientation['parameter_c']
    Results = {}
    index = 0
    for key in filename_dict:
        Results[key] = get_distribution_orientation_outside(filename = filename_dict[key], radius_core_value = radius_core_lst[index], radius_scale_value = radius_scale_lst[index])
        index = index + 1
    return Results

def get_distribution_orientation_outside(filename = None, radius_core_value = None, radius_scale_value = None, **kwargs):
    ##########################
    ## Extract data ##########
    data = poda.get_data(filename)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    filament_number = poda.get_filament_number(data)
    filament_length = poda.get_filament_length(data)
    point_number_filament = poda.get_point_number_filament(data)
    elastic_energy = poda.get_local_energy(data)#sum of local energy
    local_energy = poda.get_loc_energy(data)#local energy
    ## Get dictionnary containing lists of filament orientation inside the podosome core
    dict = poda.get_filament_orientation_outside(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filament_length = filament_length, local_energy = local_energy, filename = filename, core_radius = radius_core_value, radius_scale = radius_scale_value)
    return dict

############################################################
## Distribution of filament length inside the core #########
def get_results_distribution_length_core(podosome_characteristics = None, **kwargs):
    ## Get dictionnary of filenames
    data_podosome = pd.read_csv(podosome_characteristics) ## Read Input file
    filename_dict = get_filename_dict(data_podosome['filename']) #Creates a dictionnary containing all input filenames
    ## Read fit parameters
    data_fit_length = pd.read_csv('./results/general/length_fit_parameters.csv')
    radius_core_lst =  data_fit_length['radius_core']
    radius_scale_lst =  data_fit_length['parameter_c']
    Results = {}
    index = 0
    for key in filename_dict:
        Results[key] = get_distribution_length_core(filename = filename_dict[key], radius_core_value = radius_core_lst[index], radius_scale_value = radius_scale_lst[index])
        index = index + 1
    return Results

def get_distribution_length_core(filename = None, radius_core_value = None, radius_scale_value = None, **kwargs):
    ##########################
    ## Extract data ##########
    data = poda.get_data(filename)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    filament_number = poda.get_filament_number(data)
    filament_length = poda.get_filament_length(data)
    point_number_filament = poda.get_point_number_filament(data)
    elastic_energy = poda.get_local_energy(data)#sum of local energy
    local_energy = poda.get_loc_energy(data)#local energy
    ## Get dictionnary containing lists of filament length inside the podosome core
    dict = poda.get_filament_length_core(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filament_length = filament_length, local_energy = local_energy, filename = filename, core_radius = radius_core_value, radius_scale = radius_scale_value)
    return dict

############################################################
## Distribution of filament length outside the core ########
def get_results_distribution_length_outside(podosome_characteristics = None, **kwargs):
    ## Get dictionnary of filenames
    data_podosome = pd.read_csv(podosome_characteristics) ## Read Input file
    filename_dict = get_filename_dict(data_podosome['filename']) #Creates a dictionnary containing all input filenames
    ## Read fit parameters
    data_fit_length = pd.read_csv('./results/general/length_fit_parameters.csv')
    radius_core_lst =  data_fit_length['radius_core']
    radius_scale_lst =  data_fit_length['parameter_c']
    Results = {}
    index = 0
    for key in filename_dict:
        Results[key] = get_distribution_length_outside(filename = filename_dict[key], radius_core_value = radius_core_lst[index], radius_scale_value = radius_scale_lst[index])
        index = index + 1
    return Results

def get_distribution_length_outside(filename = None, radius_core_value = None, radius_scale_value = None, **kwargs):
    ##########################
    ## Extract data ##########
    data = poda.get_data(filename)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    filament_number = poda.get_filament_number(data)
    filament_length = poda.get_filament_length(data)
    point_number_filament = poda.get_point_number_filament(data)
    elastic_energy = poda.get_local_energy(data)#sum of local energy
    local_energy = poda.get_loc_energy(data)#local energy
    ## Get dictionnary containing lists of filament length inside the podosome core
    dict = poda.get_filament_length_outside(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filament_length = filament_length, local_energy = local_energy, filename = filename, core_radius = radius_core_value, radius_scale = radius_scale_value)
    return dict

###############################################
##### Core Analysis: Force, Pressure and Energy
###############################################

################################
## Polymerization force ########
def get_results_polymerization_force(podosome_characteristics = None, **kwargs):
    data_podosome = pd.read_csv(podosome_characteristics) ## Read Input file
    filename_dict = get_filename_dict(data_podosome['filename']) #Creates a dictionnary containing all input filenames
    radius_core_lst =  data_podosome['radius_core']
    Results_polymerization_force = {}
    index = 0
    for key in filename_dict:
        Results_polymerization_force[key] = get_polymerization_force_core(filename = filename_dict[key], radius_core_value = radius_core_lst[index]) #Average filament orientation for core filaments close to the membrane
        index = index + 1
    return Results_polymerization_force

def get_polymerization_force_core(filename = None, radius_core_value = None, **kwargs):
    data = poda.get_data(filename)
    nb_pts_distance_range = poda.get_number_pts_close_membrane(data)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    filament_number = poda.get_filament_number(data)
    point_number_filament = poda.get_point_number_filament(data)
    Orientation_filaments_membrane, Height_filaments_membrane, Radial_distance_membrane = poda.get_orientation_all_filaments_membrane(x_position, y_position, z_position, filament_number, point_number_filament, nb_pts_distance_range, filename)
    avg_orientation_filament_membrane, std_orientation_filament_membrane, number_filament_membrane_select, polymerization_force = poda.get_average_orientation_filaments_membrane_with_selection_radial_distance(Orientation_filaments_membrane,Radial_distance_membrane,radius_core_value)
    return {'avg_orientation_filament_membrane':avg_orientation_filament_membrane,'std_orientation_filament_membrane':std_orientation_filament_membrane,'number_filament_membrane':number_filament_membrane_select, 'polymerization_force' : polymerization_force}

#########################
## Elastic force ########
def get_results_elastic_force(podosome_characteristics = None, **kwargs):
    data_podosome = pd.read_csv(podosome_characteristics) ## Read Input file
    filename_dict = get_filename_dict(data_podosome['filename']) #Creates a dictionnary containing all input filenames
    radius_core_lst =  data_podosome['radius_core']
    height_core_lst = data_podosome['core_height']
    Results_elastic_force = {}
    index = 0
    for key in filename_dict:
        Results_elastic_force[key] = get_elastic_force_core_dict(filename = filename_dict[key], radius_core_value = radius_core_lst[index], height_core_value = height_core_lst[index])
        index = index + 1
    return Results_elastic_force

def get_elastic_force_core_dict(filename = None, radius_core_value = None, height_core_value = None, **kwargs):
    ##########################
    ## Extract data ##########
    data = poda.get_data(filename)
    x_position, y_position, z_position = poda.get_point_coordinates(data)
    filament_number = poda.get_filament_number(data)
    filament_length = poda.get_filament_length(data)
    point_number_filament = poda.get_point_number_filament(data)
    elastic_energy = poda.get_local_energy(data)#sum of local energy
    local_energy = poda.get_loc_energy(data)#local energy
    x_core, y_core = poda.get_core_coordinates(filename)
    radial_distance = poda.get_radial_distance_all_filament(x_position, y_position, x_core, y_core)
    ##############################################################################
    ## Compute the total filament length and number of monomers inside the core ##
    monomers_core_dict = poda.get_total_monomers_core_dict(filament_number = filament_number, point_number_filament = point_number_filament, actin_monomer_size = __ACTIN_MONOMER_SIZE__, filament_length = filament_length, radial_distance = radial_distance, core_radius = radius_core_value)
    ##############################################################
    ## Compute the average filament compression inside the core ##
    compression_core_dict = poda.get_average_filament_compression_core(x_position = x_position, y_position = y_position, z_position = z_position, filament_number =  filament_number, radial_distance = radial_distance, core_radius = radius_core_value, filament_length = filament_length, point_number_filament = point_number_filament, filename = filename)
    print("core has radius %s and compression %s" %(radius_core_value,compression_core_dict))

    ###############################
    ## Compute the bucling force ##
    number_filament_core = poda.get_number_filament_core(filament_number = filament_number, radial_distance = radial_distance, core_radius = radius_core_value)
    buckling_force = poda.get_buckling_force(number_of_filaments = number_filament_core, rigidity_actin = __RIGIDITY_ACTIN_FILAMENT__, filament_length = (compression_core_dict['filament_length_core'])*1e-9) #in Newtons
    ############################################
    ## Compute elastic energy inside the core ##
    elastic_energy_core = poda.get_elastic_energy_core(filament_number = filament_number, local_energy = local_energy, radial_distance = radial_distance, radius_core_value = radius_core_value)
    #############################################
    ## Compute elastic force produced by the core
    elastic_force_core  = poda.get_elastic_force(elastic_energy = elastic_energy_core, height = height_core_value*1e-9, compression = compression_core_dict['compression_filament_core']) #in Newtons
    print("core has elastic_energy %s and froce %s" % (elastic_energy_core, elastic_force_core))
    ## Compute the elastic force projected along the z-axis
    average_filament_orientation_core = poda.get_average_filament_orientation_core(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filename = filename, core_radius = radius_core_value)
    elastic_force_core_orientation_correction = elastic_force_core * np.sin((np.pi/180)*average_filament_orientation_core) #in Newtons
    ######################################################################
    ## Alternative computation of the elastic force and energy at the core
    elastic_force_and_energy_core_dict = poda.get_elastic_force_and_energy_core(x_position = x_position, y_position = y_position, z_position = z_position, filament_number = filament_number, point_number_filament = point_number_filament, filament_length = filament_length, local_energy = local_energy, filename = filename, core_radius = radius_core_value)
    elastic_force_new = elastic_force_and_energy_core_dict['total_elastic_force_core'] #Sum of all single_filament_elastic_forces
    elastic_force_corrected_new = elastic_force_and_energy_core_dict['total_elastic_force_projected_z_core'] #Sum of all single_filament_elastic_forces*sin(theta)
    elastic_energy_new = elastic_force_and_energy_core_dict['total_elastic_energy_core'] #Sum of all single_filament_elastic_energy
    buckling_force_new =  elastic_force_and_energy_core_dict['total_buckling_force_core'] #Sum of all single_filament_buckling_force
    ## Return dictionnary of results
    return {'buckling_force_new':buckling_force_new,'elastic_force_corrected_new':elastic_force_corrected_new,'elastic_force_new':elastic_force_new,'elastic_energy_new':elastic_energy_new,'elastic_force_core_orientation_correction' : elastic_force_core_orientation_correction, 'elastic_force_core' : elastic_force_core, 'buckling_force' : buckling_force, 'elastic_energy_core': elastic_energy_core, 'number_filament_core' : number_filament_core,'monomers_core_tot' : monomers_core_dict['total_monomers'], 'total_filament_length_core' : monomers_core_dict['total_filament_length']}


################################################################################################################################################################################
#### SAVE RESULTS ###############################################################################################################################################################
################################################################################################################################################################################

def save(all_results_dict = None, podosome_characteristics = None, results_filenames = None, config = None, **kwargs):
    for key in all_results_dict:
        save_results_all(dict = all_results_dict[key], tag = key, podosome_characteristics = podosome_characteristics, results_filenames = results_filenames, config = config)

def save_results_all(dict = None, tag = None, podosome_characteristics = None, results_filenames = None, config = None, **kwargs):
    if tag == 'length' or tag == 'orientation' or tag == 'filament_density' or tag == 'density_energy' or tag == 'distribution' or tag == 'length_height' or tag == 'curvature' or tag == 'compression' or tag == 'filament_orientation_core' or tag == 'filament_orientation_outside' or tag == 'filament_length_core' or tag == 'filament_length_outside':
        option = 'lists'
    elif tag == 'elastic_force' or tag == 'polymerization_force':
        option = 'numbers'
    save_results(results_dict = dict, podosome_characteristics = podosome_characteristics, tag = tag, results_filenames = results_filenames, opt = option)
    if tag == 'length' or tag == 'orientation' or tag == 'filament_density' or tag == 'density_energy' or tag == 'curvature' or tag == 'compression':
        export_final_csv_files(tag = tag, output_filename_main = 'results/general/'+tag+'.csv', output_filename_second = 'results/general/'+tag+'_fit_parameters.csv', podosome_characteristics = podosome_characteristics, results_filenames = results_filenames, config = config)

def save_results(podosome_characteristics = None, results_filenames = None, tag = None, results_dict = None, opt = None, **kwargs):
    data_podosome = pd.read_csv(podosome_characteristics) #Read podosome_csv
    data_res_filename = pd.read_csv(results_filenames)
    specific_name_lst = data_podosome['podosome_name']+'.csv' #List of podosome names
    common_name = data_res_filename[tag][0]
    k = 0
    for key in results_dict:
        if opt == 'numbers':
            res = res = pd.DataFrame(results_dict[key], index=[0])
        elif opt == 'lists':
            res = pd.DataFrame.from_dict(results_dict[key])
        res.to_csv(common_name+specific_name_lst[k]) #Export csv results files`
        k = k+1

#############################
#### Radial distance analysis
#############################

def export_final_csv_files(tag = None, config = None, output_filename_second = None, output_filename_main = None, results_filenames = None, podosome_characteristics = None, **kwargs):
    data_res_filename = pd.read_csv(results_filenames)
    #######################
    ## Export main data ###
    if tag == 'length':
        dict = get_dict_length(common_name = data_res_filename[tag][0], podosome_characteristics = podosome_characteristics)
        new_dict = get_new_dict(old_dict = dict)
    elif tag == 'orientation':
        dict = get_dict_orientation(common_name = data_res_filename[tag][0], podosome_characteristics = podosome_characteristics)
        new_dict = get_new_dict(old_dict = dict)
    elif tag == 'filament_density':
        dict = get_dict_filament_density(common_name = data_res_filename[tag][0], podosome_characteristics = podosome_characteristics)
        new_dict = get_new_dict(old_dict = dict)
    elif tag == 'density_energy':
        dict = get_dict_density_energy(common_name = data_res_filename[tag][0], podosome_characteristics = podosome_characteristics)
        new_dict = get_new_dict(old_dict = dict)
    elif tag == 'curvature':
        dict = get_dict_filament_curvature(common_name = data_res_filename[tag][0], podosome_characteristics = podosome_characteristics)
        new_dict = get_new_dict(old_dict = dict)
    elif tag == 'compression':
        dict = get_dict_filament_compression(common_name = data_res_filename[tag][0], podosome_characteristics = podosome_characteristics)
        new_dict = get_new_dict(old_dict = dict)
    data = pd.DataFrame(new_dict)
    data.to_csv(output_filename_main)
    ############################
    ## Export fit parameters ###
    if tag == 'length' or tag == 'orientation' or tag == 'filament_density' or tag == 'density_energy':
        fit_parameters_dict = get_fit_parameters_dict(results_dict = new_dict, config = config, opt = tag)
        data_fit = pd.DataFrame(fit_parameters_dict)
        data_fit.to_csv(output_filename_second)

def get_fit_parameters_dict(opt = None, config = None, results_dict = None, **kwargs):
    ####################
    ### Extract data ###
    ##Get x-values
    radius = list(np.linspace(config['Radius']['min'],config['Radius']['max'],config['Radius']['number_points']))
    interval = radius[1]-radius[0]
    radius_centered = [r+(interval/2) for r in radius]
    ##########################
    ### Get fit parameters ###
    radius_core_lst = []
    param_a_lst = []
    param_b_lst = []
    param_c_lst = []
    for key in results_dict:
        data_to_fit = results_dict[key]
        if opt == 'length':
            [radius_core,param_a,param_b,param_c] = poda.fit_data_growth(x_data = radius_centered, y_data = data_to_fit, bounds_tuple = ([0,0,0,0],[max(radius_centered),1e3,1e3,1e3]))
        elif opt == 'density_energy':
            [radius_core,param_a,param_b,param_c] = poda.fit_data_decay(x_data = radius_centered, y_data = data_to_fit, bounds_tuple = ([0,0,0,0],[max(radius_centered),2e6,2e6,1e3]))
        elif opt == 'filament_density':
            [radius_core,param_a,param_b,param_c] = poda.fit_data_decay(x_data = radius_centered, y_data = data_to_fit, bounds_tuple = ([0,0,0,0],[max(radius_centered),1e5,1e5,1e3]))
        elif opt == 'orientation':
            [radius_core,param_a,param_b,param_c] = poda.fit_data_decay(x_data = radius_centered, y_data = data_to_fit, bounds_tuple = ([0,0,0,0],[max(radius_centered),1e3,1e3,1e3]))
        radius_core_lst.append(radius_core)
        param_a_lst.append(param_a)
        param_b_lst.append(param_b)
        param_c_lst.append(param_c)
    return {'radius_core' : radius_core_lst, 'parameter_a' : param_a_lst, 'parameter_b' : param_b_lst, 'parameter_c' : param_c_lst}

def get_dict_filament_curvature(podosome_characteristics = None, common_name = None, **kwargs):
    ######################################################
    ## Extract list of podosome names from podosome_csv ##
    data_podosome = pd.read_csv(podosome_characteristics) #Get podosome characteristics
    specific_name_lst = data_podosome['podosome_name']+'.csv'#List of podosome names
    ###############################################
    ## Get dictionnary of results from csv files ##
    dict = {}
    for filename in specific_name_lst:
        data = pd.read_csv(common_name+filename)
        dict[filename] = {'AVG_curvature' : data['AVG_curvature'],'STD_curvature' : data['STD_curvature'],'Radius' : data['Radius_lst']}
    return dict

def get_dict_filament_compression(podosome_characteristics = None, common_name = None, **kwargs):
    ######################################################
    ## Extract list of podosome names from podosome_csv ##
    data_podosome = pd.read_csv(podosome_characteristics) #Get podosome characteristics
    specific_name_lst = data_podosome['podosome_name']+'.csv'#List of podosome names
    ###############################################
    ## Get dictionnary of results from csv files ##
    dict = {}
    for filename in specific_name_lst:
        data = pd.read_csv(common_name+filename)
        dict[filename] = {'AVG_compression' : data['AVG_compression'],'STD_compression' : data['STD_compression'],'Radius' : data['Radius_lst']}
    return dict

def get_dict_density_energy(podosome_characteristics = None, common_name = None, **kwargs):
    ######################################################
    ## Extract list of podosome names from podosome_csv ##
    data_podosome = pd.read_csv(podosome_characteristics) #Get podosome characteristics
    specific_name_lst = data_podosome['podosome_name']+'.csv'#List of podosome names
    ###############################################
    ## Get dictionnary of results from csv files ##
    dict = {}
    for filename in specific_name_lst:
        data = pd.read_csv(common_name+filename)
        dict[filename] = {'density_elastic_energy' : [value/(4e-21*1e-9) for value in data['density_elastic_energy_volume']],'Radius' : data['Radius_lst']}
    return dict

def get_dict_filament_density(podosome_characteristics = None, common_name = None, **kwargs):
    ######################################################
    ## Extract list of podosome names from podosome_csv ##
    data_podosome = pd.read_csv(podosome_characteristics) #Get podosome characteristics
    specific_name_lst = data_podosome['podosome_name']+'.csv'#List of podosome names
    ###############################################
    ## Get dictionnary of results from csv files ##
    dict = {}
    for filename in specific_name_lst:
        data = pd.read_csv(common_name+filename)
        dict[filename] = {'density' : data['density'],'Radius' : data['Radius_lst']}
    return dict

def get_dict_orientation(podosome_characteristics = None, common_name = None, **kwargs):
    ######################################################
    ## Extract list of podosome names from podosome_csv ##
    data_podosome = pd.read_csv(podosome_characteristics) #Get podosome characteristics
    specific_name_lst = data_podosome['podosome_name']+'.csv'#List of podosome names
    ###############################################
    ## Get dictionnary of results from csv files ##
    dict = {}
    for filename in specific_name_lst:
        data = pd.read_csv(common_name+filename) #Filament orientation as a function of the radius
        dict[filename] = {'AVG_orientation' : data['AVG_orientation'],'STD_orientation' : data['STD_orientation'],'SEM_orientation' : data['SEM_orientation'], 'Radius' : data['Radius_lst']}
    return dict

def get_dict_length(podosome_characteristics = None, common_name = None, **kwargs):
    ######################################################
    ## Extract list of podosome names from podosome_csv ##
    data_podosome = pd.read_csv(podosome_characteristics) #Get podosome characteristics
    specific_name_lst = data_podosome['podosome_name']+'.csv'#List of podosome names
    ###############################################
    ## Get dictionnary of results from csv files ##
    dict = {}
    for filename in specific_name_lst:
        data = pd.read_csv(common_name+filename) #Filament length as a function of the radius
        dict[filename] = {'AVG_length' : data['AVG_length'],'STD_length' : data['STD_length'],'SEM_length' : data['SEM_length'], 'Radius' : data['Radius_lst']}
    return dict

def get_new_dict(old_dict = None, **kwargs):
    new_dict = {}
    for key in old_dict:
        data_col_name = str(list(old_dict[key].keys())[0])
        split_string = key.split(".", 1)
        new_key = split_string[0]
        new_dict[new_key] = old_dict[key][data_col_name]
    return new_dict

###################################################
##### Further analysis of  filament organization ##
###################################################

def get_dict_length_height_core(podosome_characteristics = None, common_name = None, **kwargs):
    ######################################################
    ## Extract list of podosome names from podosome_csv ##
    data_podosome = pd.read_csv(podosome_characteristics) #Get podosome characteristics
    specific_name_lst = data_podosome['podosome_name']+'.csv'#List of podosome names
    ###############################################
    ## Get dictionnary of results from csv files ##
    dict = {}
    for filename in specific_name_lst:
        data = pd.read_csv(common_name+filename) #Filament length as a function of the radius
        dict[filename] = {'AVG_length' : data['AVG_length'],'STD_length' : data['STD_length'],'AVG_height' : data['AVG_height'], 'AVG_height_percent' : data['AVG_height_percent']}
    return dict

def get_dict_distribution_length_core(podosome_characteristics = None, common_name = None, **kwargs):
    ######################################################
    ## Extract list of podosome names from podosome_csv ##
    data_podosome = pd.read_csv(podosome_characteristics) #Get podosome characteristics
    specific_name_lst = data_podosome['podosome_name']+'.csv'#List of podosome names
    ###############################################
    ## Get dictionnary of results from csv files ##
    dict = {}
    for filename in specific_name_lst:
        data = pd.read_csv(common_name+filename)
        dict[filename] = {'filament_length_core' : data['filament_length_core']}
    return dict

def get_dict_distribution_length_outside(podosome_characteristics = None, common_name = None, **kwargs):
    ######################################################
    ## Extract list of podosome names from podosome_csv ##
    data_podosome = pd.read_csv(podosome_characteristics) #Get podosome characteristics
    specific_name_lst = data_podosome['podosome_name']+'.csv'#List of podosome names
    ###############################################
    ## Get dictionnary of results from csv files ##
    dict = {}
    for filename in specific_name_lst:
        data = pd.read_csv(common_name+filename)
        dict[filename] = {'filament_length_outside' : data['filament_length_outside']}
    return dict

def get_dict_distribution_orientation_core(podosome_characteristics = None, common_name = None, **kwargs):
    ######################################################
    ## Extract list of podosome names from podosome_csv ##
    data_podosome = pd.read_csv(podosome_characteristics) #Get podosome characteristics
    specific_name_lst = data_podosome['podosome_name']+'.csv'#List of podosome names
    ###############################################
    ## Get dictionnary of results from csv files ##
    dict = {}
    for filename in specific_name_lst:
        data = pd.read_csv(common_name+filename)
        dict[filename] = {'filament_orientation_core' : data['filament_orientation_core']}
    return dict

def get_dict_distribution_orientation_outside(podosome_characteristics = None, common_name = None, **kwargs):
    ######################################################
    ## Extract list of podosome names from podosome_csv ##
    data_podosome = pd.read_csv(podosome_characteristics) #Get podosome characteristics
    specific_name_lst = data_podosome['podosome_name']+'.csv'#List of podosome names
    ###############################################
    ## Get dictionnary of results from csv files ##
    dict = {}
    for filename in specific_name_lst:
        data = pd.read_csv(common_name+filename)
        dict[filename] = {'filament_orientation_outside' : data['filament_orientation_outside']}
    return dict

###################################################
##### Force generation from the core compression ##
###################################################

def add_core_radius_to_podosome_csv(col_name = None, results_fit_parameters = None, podosome_characteristics = None, **kwargs):
    data = merge_data(pd.read_csv(podosome_characteristics), pd.read_csv(results_fit_parameters), col_name)
    data.to_csv(podosome_characteristics, index=False)

def export_forces_pressure_energy_to_csv(results_force_energy_pressure = None, podosome_characteristics = None, results_filenames = None, **kwargs):
    data_res_filename = pd.read_csv(results_filenames) #Read res_filenames_csv
    elastic_dict = get_dict_elastic_force(data_res_filename['elastic_force'][0],podosome_characteristics)
    polymerization_dict = get_dict_polymerization_force(data_res_filename['polymerization_force'][0], podosome_characteristics)
    dict = get_forces_pressure_energy_dict(elastic_dict, polymerization_dict, podosome_characteristics)
    data = pd.DataFrame(dict)
    pressure_exp,err = get_pressure_exp()
    data['pressure_exp'] = pressure_exp
    data['pressure_exp_err'] = err
    outputfilename = results_force_energy_pressure
    data.to_csv(outputfilename)

def get_dict_elastic_force(common_name,podosome_csv):
    ## Extract list of podosome names from podosome_csv
    data_podosome = pd.read_csv(podosome_csv) #Read podosome_csv
    specific_name_lst = data_podosome['podosome_name']+'.csv' #List of podosome names
    ## Get dictionnary of results from csv files
    dict = {}
    for filename in specific_name_lst:
        data = pd.read_csv(common_name+filename)
        dict[filename] = {'buckling_force_new':data['buckling_force_new'],'elastic_force_new':data['elastic_force_new'],'elastic_force_corrected_new':data['elastic_force_corrected_new'],'elastic_energy_new':data['elastic_energy_new'],'elastic_energy_core': data['elastic_energy_core'],'elastic_force_core_orientation_correction':data['elastic_force_core_orientation_correction'],'elastic_force_core': data['elastic_force_core'],'monomers_tot' : data['monomers_core_tot'] ,'buckling_force': data['buckling_force'], 'total_filament_length_core' : data['total_filament_length_core'], 'number_filament_core' : data['number_filament_core']}
    return dict

def get_dict_polymerization_force(common_name,podosome_csv):
    ## Extract list of podosome names from podosome_csv
    data_podosome = pd.read_csv(podosome_csv) #Read podosome_csv
    specific_name_lst = data_podosome['podosome_name']+'.csv'#List of podosome names
    ## Get dictionnary of results from csv files
    polymerization_force_dict = {}
    for filename in specific_name_lst:
        data_orientation_filament_membrane_select = pd.read_csv(common_name+filename)
        polymerization_force_dict[filename] = {'AVG_orientation' : data_orientation_filament_membrane_select['avg_orientation_filament_membrane'],'STD_orientation' : data_orientation_filament_membrane_select['std_orientation_filament_membrane'],'number_filament_membrane':data_orientation_filament_membrane_select['number_filament_membrane'],'Polymerization_force' : data_orientation_filament_membrane_select['polymerization_force']}
    return polymerization_force_dict

def get_forces_pressure_energy_dict(Results_elastic_force, Results_polymerization_force, podosome_csv):
    #################################
    ## Extract core radius values  ##
    data_podosome = pd.read_csv(podosome_csv) ## Read Input file
    radius_core_lst = data_podosome['radius_core']
    #####################################
    ## Extract elastic energy & force  ##
    elastic_force_list = []
    elastic_force_new_list = []
    elastic_force_corrected_new_list = []
    elastic_force_corrected_list = []
    elastic_energy_list = []
    elastic_energy_new_list = []
    buckling_force_new_list = []
    monomers_list = []
    for key in Results_elastic_force:
        buckling_force_new_list.append(Results_elastic_force[key]['buckling_force_new'][0]*___STD_TO_NANO___)
        elastic_force_new_list.append(Results_elastic_force[key]['elastic_force_new'][0]*___STD_TO_NANO___)
        elastic_force_corrected_new_list.append(Results_elastic_force[key]['elastic_force_corrected_new'][0]*___STD_TO_NANO___)
        elastic_force_list.append(Results_elastic_force[key]['elastic_force_core'][0]*___STD_TO_NANO___)
        elastic_force_corrected_list.append(Results_elastic_force[key]['elastic_force_core_orientation_correction'][0]*___STD_TO_NANO___)#Elastic component(corrected one with the factor sin(theta))
        elastic_energy_list.append(Results_elastic_force[key]['elastic_energy_core'][0]/__KT_TO_JOULES__)
        elastic_energy_new_list.append(Results_elastic_force[key]['elastic_energy_new'][0]/__KT_TO_JOULES__)
        monomers_list.append(Results_elastic_force[key]['monomers_tot'][0])
    ###########################################
    ## Extract polymerization energy & force ##
    polymerization_force_list = []
    orientation_membrane_list = []
    std_orientation_membrane_list = []
    number_filament_membrane_list = []
    for key in Results_polymerization_force:
            polymerization_force_list.append(Results_polymerization_force[key]['Polymerization_force'][0]*___PICO_TO_NANO__) #conversion in nN
            orientation_membrane_list.append(Results_polymerization_force[key]['AVG_orientation'][0]) #Average orientation
            std_orientation_membrane_list.append(Results_polymerization_force[key]['STD_orientation'][0]) #Standard deviation for the orientation
            number_filament_membrane_list.append(Results_polymerization_force[key]['number_filament_membrane'][0])
    ###################################
    ## Compute polymerization energy ##
    polymerization_energy_list = [value * get_log_factor_for_polymerization_energy(stall_force_pN = 10) for value in monomers_list]
    ##############################################
    ## Compute polymerization & elatic pressure ##
    polymerization_pressure_list = []
    elastic_pressure_list = []
    elastic_pressure_new_list = []
    elastic_pressure_corrected_list = []
    elastic_pressure_corrected_new_list = []
    buckling_pressure_list = []
    for index in range(len(data_podosome)):
        polymerization_pressure_list.append((polymerization_force_list[index]/(np.pi*(radius_core_lst[index]**2)))*1e9*1e-3 ) #pressure in kPa
        elastic_pressure_list.append((elastic_force_list[index]/(np.pi*(radius_core_lst[index]**2)))*1e9*1e-3 ) #pressure in kPa
        elastic_pressure_new_list.append((elastic_force_new_list[index]/(np.pi*(radius_core_lst[index]**2)))*1e9*1e-3 ) #pressure in kPa
        elastic_pressure_corrected_list.append((elastic_force_corrected_list[index]/(np.pi*(radius_core_lst[index]**2)))*1e9*1e-3 ) #pressure in kPa
        elastic_pressure_corrected_new_list.append((elastic_force_corrected_new_list[index]/(np.pi*(radius_core_lst[index]**2)))*1e9*1e-3 ) #pressure in kPa
        buckling_pressure_list.append((buckling_force_new_list[index]/(np.pi*(radius_core_lst[index]**2)))*1e9*1e-3 ) #pressure in kPa
    return {'number_filament_membrane':number_filament_membrane_list,'AVG_orientation_membrane':orientation_membrane_list,'STD_orientation_membrane':std_orientation_membrane_list,'buckling_pressure':buckling_pressure_list,'buckling_force_new':buckling_force_new_list,'elastic_pressure_new':elastic_pressure_new_list,'elastic_pressure_corrected_new':elastic_pressure_corrected_new_list, 'elastic_energy_new':elastic_energy_new_list,'elastic_force_corrected_new':elastic_force_corrected_new_list,'elastic_force_new':elastic_force_new_list,'elastic_force_corrected' : elastic_force_corrected_list,'elastic_force' : elastic_force_list, 'polymerization_force' : polymerization_force_list,'elastic_pressure_corrected' : elastic_pressure_corrected_list, 'elastic_pressure' : elastic_pressure_list, 'polymerization_pressure' : polymerization_pressure_list, 'elastic_energy' : elastic_energy_list, 'polymerization_energy' : polymerization_energy_list}

def get_pressure_exp():
    pressure_exp = (__FORCE_EXP_AFM__[0]/(np.pi*(__CORE_RADIUS_EXP_AFM__[0]**2)))*1e6 #in kPa
    err_pressure_exp = ((2*__FORCE_EXP_AFM__[0]/(np.pi*((__CORE_RADIUS_EXP_AFM__[0])**3)))*__CORE_RADIUS_EXP_AFM__[1] + __FORCE_EXP_AFM__[1]/(np.pi*(__CORE_RADIUS_EXP_AFM__[0])**2))*1e6 #in kPa
    return (pressure_exp, err_pressure_exp)

def get_log_factor_for_polymerization_energy(stall_force_pN = None, **kwargs):
    return stall_force_pN*___PICO_TO_STD__*__ACTIN_MONOMER_SIZE__*___NANO_TO_STD__/(__KT_TO_JOULES__)

################################################################################################################################################################################
#### OTHER FUNCTIONS ###############################################################################################################################################################
################################################################################################################################################################################

def merge_data(data_main, data_second, col_name):
    lst = data_second[col_name]
    data_main[col_name] = lst
    return data_main

def get_filename_dict(filename_lst):
    dict = {}
    for index in range(len(filename_lst)):
        dict[index] = filename_lst[index]
    return dict
