# necessary import
import numpy as np
import pandas as pd
import re
import yaml
from scipy.optimize import curve_fit

# import only for 3D rendering
__IPV__ = False
try:
    import ipyvolume as ipv
    __IPV__ = True
except:
    __IPV__ = False

""" This files implements : 
    - Analyzer : a class providing methods to read podosome text files into a convenient data structure, 
                    and allows for 3D rendering$
    - A set of helper functions
"""

## The analyzer itself
class Analyzer:
    """
    An analyzer for a set of podosomes, introduced via a csv file characters
    This is mostly a backend for analysis :
        - podosomes are loaded into memory from files into Analyzer.podosomes

        - podosomes are dictionaries with
            'data' : the raw data as np.array
            'filaments' a list of filaments, each a N x 9 np.array (N : number of points)
            'segments' a M x 9 numpy array of all segments (M: total number of segments)

            row 0 : point number
            rows 1-3: x y z
            row 4 : filament number
            row 5 : local bending energy
            row 6 : touching the membrane
    """
    def __init__(self,*args, config=None, characters=None, analysis=None, **kwargs):
        with open(config, 'r') as stream:
            try:
                self.config = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        self.make_podosomes(characters, *args, **kwargs)
        if analysis is not None:
            self.analysis = analysis
        else:
            self.analysis = {}

    def make_podosomes(self, characters, *args, **kwargs):
        """ builds self.podosomes, a list of dictionaries describing podosomes
            each podosome is a dictionary with fields "data", "segments", "results", ... """
        chars = pd.read_csv(characters)
        self.podosomes = chars.to_dict(orient="records")
        for podosome in self.podosomes:
            self.get_podosome_data(podosome)
            self.get_core_coordinates(podosome)
            self.correct_podosome_data(podosome)
            self.sort_filaments(podosome)
            podosome["results"] = {}

    def get_core_coordinates(self, podosome):
        """ Extract core coordinates from filename"""
        # This is utterly disgusting and should be in the characteristics file
        find_x = re.search('xCoeur(\d+\.\d+)', podosome["filename"], re.IGNORECASE)
        if find_x is None:
            find_x = re.search('xCoeur(\d+)', podosome["filename"], re.IGNORECASE)
        find_y = re.search('yCoeur(\d+\.\d+)', podosome["filename"], re.IGNORECASE)
        if find_y is None:
            find_y = re.search('yCoeur(\d+)', podosome["filename"], re.IGNORECASE)
        podosome["x_core"] = float(find_x.group(1))
        podosome["y_core"] = float(find_y.group(1))

    def correct_podosome_data(self, podosome):
        """ Translates in z according to x_core, y_core ,
            and translates (+inverse if needed) in z according to z_membrane """
        tag = podosome["tomo"]
        shift = podosome["shift"]
        x_core = podosome["x_core"]
        y_core = podosome["y_core"]
        data = podosome["data"]
        shape = np.shape(data)
        col = np.ones((shape[0],), "float")
        # Correcting z axis
        if tag == 'notinverted':
            z_membrane = min(data[:, 3]) + 1.0*shift
            data[:, 3] = data[:, 3] - z_membrane * col
        elif tag == 'inverted':
            z_membrane = max(data[:, 3]) - 1.0*shift
            data[:, 3] = z_membrane * col - data[:, 3]
        # Correcting xy
        data[:, 1] -= x_core * col
        data[:, 2] -= y_core * col

    def get_podosome_data(self, podosome):
        """ Podosome is a dictionary read from characteristics"""
        filename = podosome["filename"]
        podosome["data"] = get_data(filename)
        podosome["index"] = podosome["podosome_name"].split("_")[-1]

    def sort_filaments(self, podosome):
        """ Prepares separate filaments and segments for future analysis"""
        data = podosome["data"]
        s = np.shape(data)
        difs = np.diff(data[:,4])
        changes = np.append( np.insert(np.where(difs != 0), 0, -1, axis=1 ).flatten() , s[0]-1 )
        podosome["filaments"] = []
        c = self.config["columns"]
        for j, i in enumerate(changes[:-1]):
            ordr = [c["pt_number"], c["x"], c["y"], c["z"], c["fil_number"], c["local_energy"], c["membrane"]]
            podosome["filaments"].append(data[i + 1:changes[j + 1] + 1, ordr])

        ixes = [np.shape(filament)[0]-1 for filament in podosome["filaments"]]
        segs = np.zeros(( np.sum(ixes), 8))
        indice = 0
        for i,filament in enumerate(podosome["filaments"]):
            segs[indice:indice+ixes[i], :] = self.get_segments(filament)
            indice += ixes[i]
        podosome['segments'] = segs

    def perform_analysis(self, *args, analysis=None, **kwargs):
        """ performs the analysis passed as function of a podosome
            functions are of type func(podosome, *args, analyzer = ... , **kwargs)
        """
        if analysis is not None:
            self.analysis.update(analysis)
        for key in self.analysis.keys():
            func = self.analysis[key]
            for podosome in self.podosomes:
                podosome["results"][key] = func(podosome)

    def get_segments(self, filament):
        """ segments is an N-1 x 7 vector
             with N the number of points of the filament
             column 0  is filament id
             columns 1,2,3 are position
             columns 4,5,6 are orientation
             column 7 is wether it is touching the membrane
         """
        ns = np.shape(filament)[0]
        segments = np.zeros((ns-1, 8))

        segments[:, 0] = filament[0:ns - 1, 4]
        segments[:, 1:4] = (filament[0:ns-1, 1:4] + filament[1:ns, 1:4])/2.0
        segments[:, 4:7] = filament[1:ns, 1:4] - filament[0:ns-1, 1:4]
        segments[:, 7] = filament[1:ns, 6] + filament[0:ns-1, 6]
        return segments

    def clear(self):
        """ A wrapper for ipv.clear() : clears the display """
        if __IPV__:
            ipv.clear()
        else:
            print("Cannot plot : Unable to import Ipyvolume")

    def show(self, *args, square = None, **kwargs):
        """ A wrapper for ipv.show : shows the plot """
        if __IPV__:
            ipv.show(*args, **kwargs)
            if square is None:
                square = True
            if square:
                ipv.squarelim()
        else:
            print("Cannot plot : Unable to import Ipyvolume")

    def plot_podosome(self, index, *args, filament_sorter=None, monomer_sorter=None,
                      clear=None, show=None, color=None,**kwargs):
        """ Plots a podosome, with a possible choice of filaments or monomer
         Filaments to be plotted can be chosen with the boolean function filament_sorter
         Monomers  to be plotted can be chosen with the boolean function monomer_sorter

         By default plots all filaments, with segments touching the membrane in green """
        if __IPV__:
            if clear is not None:
                if clear is True:
                    self.clear()
            podosome = self.podosomes[index]
            if filament_sorter is None:
                fils = podosome["filaments"]
            else:
                fils = filter(filament_sorter, podosome["filaments"])

            for fil in fils:
                if monomer_sorter is None:
                    if color is not None:
                        self.plot_filament(fil, *args, color=color, **kwargs)
                    else:
                        self.plot_filament(fil, *args, monomer_sorter=not_touching, **kwargs)
                        self.plot_filament(fil, *args, monomer_sorter=touching_with_continuity, color="green", **kwargs)
                else:
                    self.plot_filament(fil, *args, monomer_sorter=monomer_sorter, **kwargs)
            if show is None:
                show = True
            if show:
                self.show(*args, **kwargs)
        else:
            print("Cannot plot : Unable to import Ipyvolume")

    def plot_filament(self, filament, *args, monomer_sorter=None, **kwargs):
        """ Plots a filament, possibly choosing a subset of monomers
            if a function monomer_sorter is provided """
        if __IPV__:
            if monomer_sorter is None:
                ipv.plot(filament[:, 1], filament[:, 2], filament[:, 3], *args,  **kwargs)
            else:
                kept = monomer_sorter(filament)
                if sum(kept):
                    ipv.plot(filament[kept, 1], filament[kept, 2], filament[kept, 3], *args, **kwargs)
        else:
            print("Cannot plot : Unable to import Ipyvolume")

def get_data(filename):
    """ Reads data from file into a numpy array """
    # This was empirically the fastest
    return pd.read_csv(filename, header=None,).to_numpy()

## Functions conveniently provided for analysis
def summed_orientation_correlation(vectors):
    """ Returns the summed orientation correlation (2nd column) as a function of distance (1st column)
        3rd column is the count.

        The usual correlation is thus res[:,1]/res[:,2]
        while the distance is res[0,:]/res[:,2] """
    npts = vectors.shape[0]
    norms = np.outer(np.sqrt(np.sum(np.square(vectors), axis=1)), np.ones((1, vectors.shape[1])))
    ds = np.mean(norms[:, 0])
    vec = vectors / norms
    res = np.zeros((npts, 3))
    res[0, 1] = npts
    res[0, 2] = npts
    for i in range(1, npts):
        res[i, 0] = (i * ds) * (npts - i)
        res[i, 1] = np.sum(np.sum(vec[0:(npts - i), :] * vec[i:npts, :], axis=1))
        res[i, 2] = npts - i
    return res

def get_correlations(segments):
    """ Returns the average orientation correlation of all filaments in segments """
    segs = [segments[segments[:, 0] == ix, 4:7] for ix in np.unique(segments[:, 0])]
    lengths = [np.shape(seg)[0] for seg in segs]
    correlations = np.zeros((np.max(lengths), 3))
    for i, seg in enumerate(segs):
        correlations[0:lengths[i], :] += summed_orientation_correlation(seg)
    correlations[:, 0] /= correlations[:, 2]
    correlations[:, 1] /= correlations[:, 2]
    return correlations

def array_norm(arr):
    """ 2-Norm of an array on the horizontal axis """
    return np.sqrt(np.sum(np.power(arr, 2.0), axis=1))

def fil_length(filament):
    """ returns the length of the filament """
    return np.sum(array_norm(filament[0:-1, 1:4] - filament[1:, 1:4]))

def get_orientations(segments):
    """ Orientation of segments"""
    return (180/np.pi)*np.abs(np.arcsin(segments[:, 6]/array_norm(segments[:, 4:7])))

def in_radius(object, rmin, rmax, zmin=None, zmax=None):
    """ Selects a rows inside a radius, possibly between z_min and z_max """
    r2 = object[:, 1] ** 2.0 + object[:, 2] ** 2.0
    if zmax is None and zmin is None:
        return (r2 >= rmin**2.0) * (r2 < rmax**2.0)
    elif zmin is None:
        print(f"just zmax:{zmax}  and max:{np.max(object[:,3])}")
        return (r2 >= rmin ** 2.0) * (r2 < rmax ** 2.0) * (object[:, 3] < zmax)
    elif zmax is None:
        return (r2 >= rmin ** 2.0) * (r2 < rmax ** 2.0) * (object[:, 3] > zmin)
    else:
        return (r2 >= rmin ** 2.0) * (r2 < rmax ** 2.0) * (object[:, 3] < zmax) * (object[:, 3] > zmin)

def fit_to_step(radii, data, bounds=None):
    """ Fits a smooth step function for y=data and x=radi"""
    if bounds is None:
        bounds = ([min(radii), 0, 0, 0],[max(radii), 10000, 10000, 10000])
    params = curve_fit(smooth_step, radii, data, bounds=bounds)
    [param_x0, param_a, param_b, param_c] = params[0]
    return {"radius_core": param_x0, "parameter_a": param_a, "parameter_b": param_b, "parameter_c": param_c }

def smooth_step(x, x_0, a, b, c):
    """ A smooth step function with free lower and upper z bounds, free step x position and width """
    return 0.5*((a+b) + (b-a)*np.tanh((x-x_0)/c))

def segments_touching(segments):
    """ Finds segments touching the membrane """
    touch = np.where(segments[:, 7] > 1)[0]
    if np.any(touch):
        return segments[touch, :]
    else:
        return None

def touching_with_continuity(arr):
    """ Finds parts of filaments touching the membrane """
    choice = arr[:, 6] > 0
    if choice[-1]:
        mini = np.min(np.where(choice))
        if mini >= 1:
            choice[mini - 1] = True
    elif choice[0]:
        maxi = np.max(np.where(choice))
        if maxi + 1 < np.shape(choice)[0]:
            choice[maxi + 1] = True
    return choice

def not_touching(arr):
    """ Parts of filaments definitely not touching the membrane """
    return arr[:, 6] == 0
