 # generic import
import math
import src.podosome_analyzer as pa

import numpy as np

# Matplotlib
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['axes.linewidth'] = 1 #set the value globally

# Utilities
def get_label_and_color_lists(n):
    label_name_lst = ['#%s' %(i+1) for i in range (n)]
    initial_color_name_lst = ['red','blue','orange','gray','peru','navy','pink','green','lightblue','orchid']
    lc = len(initial_color_name_lst)
    if n < lc:
        color_name_lst = initial_color_name_lst[:n % lc]
    elif n == lc:
        color_name_lst = initial_color_name_lst
    elif n > lc:
        color_name_lst = []
        i = 0
        while i < math.floor(n/lc):
            color_name_lst.extend(initial_color_name_lst)
            i = i + 1
        color_name_lst.extend(color_name_lst[:n % lc])
    return label_name_lst, color_name_lst

# Plot functions

def plot_data_as_function_of_radius(radius_centered = None, fits = None, results = None,  filename = None, tag = None, **kwargs):
    step = pa.smooth_step
    plt.close()
    fig, ax = plt.subplots()
    label_name, color = get_label_and_color_lists(len(results)) #Get list of label names and colors
	##Plot data for all podosomes

    for index, values in enumerate(results):
        data_fit = fits[index]
        ax.scatter(radius_centered, values, marker='o',c=color[index],label=label_name[index]) #Plot data points
        radius_fit = np.linspace(min(radius_centered), max(radius_centered), num=100) #Define list of x-values for plotting the fitting lines
        if tag == 'length':
            ax.plot(radius_fit,step(radius_fit,data_fit['radius_core'],data_fit['parameter_a'],
                                    data_fit['parameter_b'],data_fit['parameter_c']),c=color[index]) #Plot fitting lines
        else:
            ax.plot(radius_fit,step(radius_fit,data_fit['radius_core'],
                                    data_fit['parameter_a'],data_fit['parameter_b'],data_fit['parameter_c']),c=color[index]) #Plot fitting lines
	##########################
	## Plot settings #########
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    ax.set_xlabel('radius (nm)',fontsize=15)
    if tag == 'orientation':
        ax.set_ylabel('Filament orientation ($^o$)',fontsize=15)
        ax.legend(fontsize=10,loc='lower left', fancybox=True, framealpha=0.8)
        ax.set_ylim([0,70])
        plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
    elif tag == 'length':
        ax.legend(fontsize=11,loc='upper left', fancybox=True)
        ax.set_ylim([100,400])
        ax.set_ylabel('Filament length (nm)',fontsize=15)
        plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
    elif tag == 'filament_density':
        ax.set_ylabel('Filament density ($\mu m /\mu m^3$)',fontsize=15)
        ax.legend(fontsize=11,loc='upper right', fancybox=True, framealpha=0.3)
        ax.set_ylim([0,3500])
        plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
    elif tag == 'density_energy':
        ax.set_ylabel('Density of elastic energy ($k_B\,T /\mu m^3$)',fontsize=15)
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax.legend(fontsize=11,loc='upper right', fancybox=True)
        plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
        ax.set_ylim([0,1.6e6])
    fig.savefig(filename)

def plot_data_radius_individual(radius_centered = None, values = None, index=None, data_fit = None, tag = None, filename=None, podosome_names =None,**kwargs):
    ##########################
    ## Plot data #############
    label_name, color = get_label_and_color_lists(index+1) #Get list of label names and colors
	##Plot data for all podosomes
    r_core = data_fit['radius_core']#core radius value
    r_s = data_fit['parameter_c'] #defines the scale of the transition
    radius_transition_beginning = r_core - r_s #radial distance for the beginning of the transition
    radius_transition_ending = r_core + r_s #radial distance for the end of the transition
    plt.close()
    fig, ax = plt.subplots()
    ### General plot settings
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    ax.set_xlabel('radius (nm)',fontsize=15)
    ### Plot data and fitting lines
    step = pa.smooth_step
    radius_fit = np.linspace(min(radius_centered), max(radius_centered), num=100) #Define list of x-values for plotting the fitting lines
    if tag == 'length':
        ax.fill_between([radius_transition_beginning, radius_transition_ending],[0, 0],[5e6,5e6], color="green", alpha=0.18)
        ax.scatter(radius_centered, values, marker='o',c="green",label=label_name[index]) #Plot data points
        ax.plot(radius_fit,step(radius_fit,data_fit['radius_core'],data_fit['parameter_a'],data_fit['parameter_b'],data_fit['parameter_c']),c="green") #Plot fitting lines
        ax.set_ylim([100,400])
        ax.set_ylabel('Filament length (nm)',fontsize=15)
        plt.text(320,190,"$r_0$ = {:.2f}".format(r_core)+" nm",fontsize=15)
        plt.text(320,165,"$r_s$ = {:.2f}".format(r_s)+" nm",fontsize=15)
        plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
    elif tag == 'orientation':
        ax.fill_between([radius_transition_beginning, radius_transition_ending],[0, 0],[5e6,5e6], color="black", alpha=0.18)
        ax.scatter(radius_centered, values, marker='o',c="black",label=label_name[index]) #Plot data points
        ax.plot(radius_fit,step(radius_fit,data_fit['radius_core'],data_fit['parameter_a'],data_fit['parameter_b'],data_fit['parameter_c']),c="black") #Plot fitting lines
        ax.set_ylabel('Filament orientation ($^o$)',fontsize=15)
        ax.set_ylim([0,70])
        plt.text(50,8,"core",fontsize=15)
        plt.text(350,8,"outside",fontsize=15)
        plt.text(320,60,"$r_0$ = {:.2f}".format(r_core)+" nm",fontsize=15)
        plt.text(320,55,"$r_s$ = {:.2f}".format(r_s)+" nm",fontsize=15)
        plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
    elif tag == 'filament_density':
        ax.fill_between([radius_transition_beginning, radius_transition_ending],[0, 0],[5e6,5e6], color="orange", alpha=0.18)
        ax.scatter(radius_centered, values, marker='o',c="orange",label=label_name[index]) #Plot data points
        ax.plot(radius_fit,step(radius_fit,data_fit['radius_core'],data_fit['parameter_a'],data_fit['parameter_b'],data_fit['parameter_c']),c="orange") #Plot fitting lines #Plot fitting lines
        ax.set_ylabel('Filament density ($\mu m /\mu m^3$)',fontsize=15)
        ax.set_ylim([0,3000])
        plt.text(320,2500,"$r_0$ = {:.2f}".format(r_core)+" nm",fontsize=15)
        plt.text(320,2300,"$r_s$ = {:.2f}".format(r_s)+" nm",fontsize=15)
        plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
    elif tag == 'density_energy':
        ax.fill_between([radius_transition_beginning, radius_transition_ending],[0, 0],[5e6,5e6], color="magenta", alpha=0.18)
        ax.scatter(radius_centered, values, marker='o',c="magenta",label=label_name[index]) #Plot data points
        ax.plot(radius_fit,step(radius_fit,data_fit['radius_core'],data_fit['parameter_a'],data_fit['parameter_b'],data_fit['parameter_c']),c="magenta") #Plot fitting lines#Plot fitting lines
        ax.set_ylabel('Density of elastic energy ($k_B\,T /\mu m^3$)',fontsize=15)
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        plt.text(320,1.4e6,"$r_0$ = {:.2f}".format(r_core)+" nm",fontsize=15)
        plt.text(320,1.26e6,"$r_s$ = {:.2f}".format(r_s)+" nm",fontsize=15)
        plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
        ax.set_ylim([0,1.6e6])

    fig.savefig(filename)



def plot_core_radius_comparison_all(radius_orient = None, radius_length = None, radius_density = None, radius_energy = None, filename = None, **kwargs):
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    n_podos = len(radius_orient)
    x = [k+1 for k in list(range(n_podos))]#Define x-axis values
    label_name, color = get_label_and_color_lists(n_podos) #Get list of label names and colors
    ax.scatter(x,radius_orient,linestyle='None',c='black',marker='o',s=60, label='orientation')
    ax.scatter(x,radius_length,linestyle='None',c='green',marker='o',s=60, label='length')
    ax.scatter(x,radius_energy,linestyle='None',c='magenta',marker='o',s=60, label='density of energy')
    ax.scatter(x,radius_density,linestyle='None',c='orange',marker='o',s=60, label='filament density')
    ##########################
	## Plot settings #########
    ax.legend(fontsize=10,loc='upper right', fancybox=True, framealpha=0.8)
    plt.xticks(x, label_name, rotation=35, fontsize=16)
    plt.yticks(fontsize=17)
    ax.set_ylim([0,500])
    ax.set_xlim([x[0]-1,x[len(x)-1]+1])
    plt.subplots_adjust(bottom=0.15,left=0.15,right=0.95)
    plt.ylabel('Core radius (nm)',fontsize=20)
    fig.savefig(filename)

def plot_core_radius_comparison_average_all(radius_orient = None, radius_length = None, radius_density = None, radius_energy = None, filename = None, **kwargs):

    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    x = [k + 1 for k in list(range(5))]  # Define x-axis values
    label_name = ["orientation", "length", "density of \n energy", "filament \n density", "exp value \n from PFM"]
    ax.errorbar([x[0]],[np.average(radius_orient)],yerr=[np.std(radius_orient)] ,linestyle='None',c='black',marker='o',ms = 10,elinewidth=2)
    ax.errorbar([x[1]],[np.average(radius_length)],yerr=[np.std(radius_length)] ,linestyle='None',c='green',marker='o',ms = 10,elinewidth=2)
    ax.errorbar([x[2]],[np.average(radius_energy)],yerr=[np.std(radius_energy)] ,linestyle='None',c='magenta',marker='o',ms = 10,elinewidth=2)
    ax.errorbar([x[3]],[np.average(radius_density)],yerr=[np.std(radius_density)] ,linestyle='None',c='orange',marker='o',ms = 10,elinewidth=2)
    ax.errorbar([x[4]],[140],yerr=[22.4] ,linestyle='None',c='red',marker='^',ms = 10,elinewidth=2)
    ##########################
    ## Plot settings #########
    plt.xticks(x, label_name, rotation=45, fontsize=13)
    plt.yticks(fontsize=17)
    ax.set_ylim([0,300])
    ax.set_xlim([x[0]-1,x[len(x)-1]+1])
    plt.subplots_adjust(bottom=0.22,top=0.95,left=0.15,right=0.95)
    plt.ylabel('Core radius (nm)',fontsize=20)
    fig.savefig(filename)

def plot_core_radius(radii = None, filename = None):
    plt.close()
    fig, ax = plt.subplots()
    n_podos = len(radii)
    x = [k+1 for k in list(range(n_podos))]#Define x-axis values
    label_name, color = get_label_and_color_lists(n_podos) #Get list of label names and colors
    for i,rad in enumerate(radii):
        ax.scatter(x[i],rad,linestyle='None',c=color[i],marker='^',s=100) #Plot data points
    ax.hlines(np.mean(radii),x[0],x[len(x)-1],colors='black',linestyle='dotted', lw=2) #Plot average value as an horizontal line
    ##########################
	## Plot settings #########
    plt.xticks(x, label_name, rotation=35, fontsize=16)
    plt.yticks(fontsize=17)
    ax.set_ylim([0,300])
    ax.set_xlim([x[0]-0.5,x[len(x)-1]+0.5])
    plt.margins(0.2)
    plt.subplots_adjust(bottom=0.3,left=0.25,right=0.85)
    plt.ylabel('core radius (nm)',fontsize=20)
    fig.savefig(filename)


def plot_filament_orientation(orientations= None, filename=None ):
    plt.close()
    fig, ax = plt.subplots()
    n_podos = len(orientations)
    label_name, color = get_label_and_color_lists(n_podos) #Get list of label names and colors
    i = 0
    AVG_ORIENTATION = []
    for i,arr in enumerate(orientations):
        x = (i+1.0)*np.ones(np.shape(arr))
        ax.scatter(x,arr, c=color[i], alpha = 0.05, s=20)
        ax.scatter([i+1],[np.average(arr)], c=color[i], label = label_name[i], s=100, marker = 's')

    x_axis = [k+1 for k in list(range(n_podos))]#Define x-axis values
    ##Plot settings
    ax.set_xlim([0,13.5])
    ax.set_ylim([-5,95])
    plt.xticks(x_axis, label_name, rotation=40,fontsize=10)
    ax.set_ylabel('Filament orientation ($°$)',fontsize=15)
    ax.legend(fontsize=11,loc='upper right', fancybox=True)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    plt.subplots_adjust(bottom=0.15,left=0.15,right=0.98)
    ax.yaxis.set_major_locator(MultipleLocator(20))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.yaxis.set_minor_locator(MultipleLocator(10))
    fig.savefig(filename)


def plot_filament_length(lengths= None, filename=None ):
    plt.close()
    fig, ax = plt.subplots()
    n_podos = len(lengths)
    label_name, color = get_label_and_color_lists(n_podos) #Get list of label names and colors
    i = 0
    for i,arr in enumerate(lengths):
        x = (i+1.0)*np.ones(np.shape(arr))
        ax.scatter(x,arr, c=color[i], alpha = 0.05, s=20)
        ax.scatter([i+1],[np.average(arr)], c=color[i], label = label_name[i], s=100, marker = 's')

    x_axis = [k+1 for k in list(range(n_podos))]#Define x-axis values
    ##Plot settings
    ax.set_xlim([0, 13.5])
    ax.set_ylim([-5, 1000])
    plt.xticks(x_axis, label_name, rotation=40, fontsize=10)
    ax.set_ylabel('Filament length (nm)', fontsize=15)
    ax.legend(fontsize=11, loc='upper right', fancybox=True)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    plt.subplots_adjust(bottom=0.15, left=0.15, right=0.98)
    ax.yaxis.set_major_locator(MultipleLocator(200))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.yaxis.set_minor_locator(MultipleLocator(100))
    fig.savefig(filename)


def plot_filament_orientation_distribution(orientations = None, filename=None, color=None, capos=None ):
    if color is None:
        color = "red"
    if capos is None:
        capos = "upper right"
    plt.close()
    fig, ax = plt.subplots()

    n, bins, patches = ax.hist(orientations,bins=49, density=True, label='average: {:.2f}'.format(np.mean(orientations))+' °', color=color,ec = 'black', alpha = 0.7, lw = 2) #The histogram of the data
    ##Plot settings
    plt.axvline(x=np.median(orientations),c="black", linestyle="dotted",lw=3,label='median: {:.2f}'.format(np.median(orientations))+' °')
    ax.set_ylabel('Probability density',fontsize=15)
    ax.set_xlabel('Filament orientation (°)',fontsize=15)
    ax.legend(fontsize=11,loc=capos, fancybox=True)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    plt.subplots_adjust(bottom=0.15,left=0.2,right=0.98)
    ax.xaxis.set_major_locator(MultipleLocator(10))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.xaxis.set_minor_locator(MultipleLocator(5))
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    ax.set_ylim([0,4.6e-2])
    fig.savefig(filename)



def plot_filament_length_distribution(lengths = None, filename=None, color=None, capos=None ):
    if color is None:
        color = "red"
    if capos is None:
        capos = "upper right"
    plt.close()
    fig, ax = plt.subplots()
    n, bins, patches = ax.hist(lengths,bins=22, density=True, label='average: {:.2f}'.format(np.mean(lengths))+' nm', color=color,ec = 'black', alpha = 0.7, lw = 2) #The histogram of the data
    ##Plot settings
    plt.axvline(x=np.median(lengths),c="black", linestyle="dotted",lw=3,label='median: {:.2f}'.format(np.median(lengths))+' nm')
    ax.set_ylabel('Probability density', fontsize=15)
    ax.set_xlabel('Filament length (nm)', fontsize=15)
    ax.legend(fontsize=11, loc='upper right', fancybox=True)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    plt.subplots_adjust(bottom=0.15, left=0.2, right=0.95)
    ax.xaxis.set_major_locator(MultipleLocator(200))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.xaxis.set_minor_locator(MultipleLocator(50))
    ax.yaxis.set_major_locator(MultipleLocator(0.3e-2))
    ax.yaxis.set_minor_locator(MultipleLocator(0.15e-2))
    ax.set_xlim([0, 1000])
    ax.set_ylim([0, 1.7e-2])
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
    fig.savefig(filename)


def plot_compression(radius_centered = None, results = None, filename = None, **kwargs):
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    label_name, color = get_label_and_color_lists(len(results)) #Get list of label names and colors
    for index, name in enumerate(label_name):
        ax.scatter(radius_centered, [value*100 for value in results[index]], marker='o',c=color[index],label=name) #Plot data points
	##########################
	## Plot settings #########
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.tick_params(axis="y", labelsize=15)
    ax.tick_params(axis="x", labelsize=15)
    ax.set_xlabel('radius (nm)',fontsize=15)
    ax.set_ylabel('Compressive strain (%)',fontsize=15)
    ax.set_ylim([0,4.0])
    #ax.legend(fontsize=11,loc='lower left', fancybox=True)
    plt.subplots_adjust(bottom=0.15,left=0.2,right=0.9)
    ax.yaxis.set_major_locator(MultipleLocator(2))
    ax.yaxis.set_minor_locator(MultipleLocator(1))
    fig.savefig(filename)

def plot_generic(radius_centered=None, results=None, filename=None, xlabel=None, ylabel=None, **kwargs):
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    label_name, color = get_label_and_color_lists(len(results))  # Get list of label names and colors
    for index, name in enumerate(label_name):
        ax.scatter(radius_centered, [value for value in results[index]], marker='o', c=color[index],
                   label=name)  # Plot data points
    ax.set_xlabel(xlabel, fontsize=15)
    ax.set_ylabel(ylabel, fontsize=15)
    fig.savefig(filename)

def plot_minimal(x=None, y=None, filename=None, xlabel=None, ylabel=None, **kwargs):
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    ax.scatter(x, y, marker='o')# Plot data points
    ax.set_xlabel(xlabel, fontsize=15)
    ax.set_ylabel(ylabel, fontsize=15)
    fig.savefig(filename)


def plot_in_out(values_in=None, values_out=None, filename = None, ymax=None, ymin=None, ylabel=None, islog=None, xlabel=None, **kwargs):
    ##########################
    ## Plot data ############
    plt.close()
    ld = len(values_in)
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range(ld))]#Define x-axis values
    label_name = get_label_and_color_lists(ld)[0]
    ax.scatter(x,values_in,c='red',s=50, label='Core',marker='^')#ax.scatter(x,buckling_force_list,c='green', label='Euler buckling',marker='D')
    ax.scatter(x,values_out,c='green',s=60, label='Radial',marker='o')
    ##########################
	## Plot settings #########
    plt.xticks(x, label_name, rotation=40, fontsize=12)
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    ax.set_xlabel(xlabel,fontsize=15)
    ax.set_ylabel(ylabel, fontsize=15)
    ax.tick_params(axis="y", labelsize=14)
    ax.tick_params(axis="x", labelsize=14)
    ax.legend(fontsize=11, fancybox=True)
    if ymin is not None and ymax is not None:
        ax.set_ylim([ymin, ymax])
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    if islog is not None:
        ax.set_yscale("log")
    fig.savefig(filename)


def plot_corr_in_out(values_in=None, values_out=None, filename = None, ymax=None, ymin=None, ylabel=None, islog=None, xlabel=None, **kwargs):
    ##########################
    ## Plot data ############
    ymax=11
    ymin=1.0
    islog = True
    plt.close()
    ld = len(values_in)
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range(ld))]#Define x-axis values
    label_name = get_label_and_color_lists(ld)[0]
    ax.scatter(x,values_in,c='red',s=50, label='Core',marker='^')#ax.scatter(x,buckling_force_list,c='green', label='Euler buckling',marker='D')
    ax.scatter(x,values_out,c='green',s=60, label='Outside',marker='o')
    FORCE_VALUE_EXP = [10.0, 3.8]
    ax.hlines(FORCE_VALUE_EXP[0], x[0], x[len(x) - 1], colors='royalblue', linestyle='dotted', label='Thermal fluctuations')
	## Plot settings #########
    plt.xticks(x, label_name, rotation=40, fontsize=12)
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    ax.set_xlabel(xlabel,fontsize=15)
    ax.set_ylabel(ylabel, fontsize=15)
    ax.tick_params(axis="y", labelsize=14)
    ax.tick_params(axis="x", labelsize=14)
    ax.legend(fontsize=11, fancybox=True)
    if ymin is not None and ymax is not None:
        ax.set_ylim([ymin, ymax])
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    if islog is not None:
        ax.set_yscale("log")
    fig.savefig(filename)

def plot_forces(forces_el=None, forces_mb=None, filename = None, **kwargs):
    ##########################
    ## Plot data ############
    plt.close()
    ld = len(forces_el)
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range(ld))]#Define x-axis values
    label_name = get_label_and_color_lists(ld)[0]
    forces_01 = [x * 0.1 for x in forces_mb]
    ax.scatter(x, forces_el, c='black', s=50, label='Elastic', marker='o')
    ax.scatter(x, forces_mb, c='orchid', s=60, label='Polymerization (10 pN)', marker='o')
    ax.scatter(x, forces_01, c='cyan', s=60, label='Polymerization (1 pN)', marker='o')
    FORCE_VALUE_EXP = [10.4,3.8]
    ax.hlines(FORCE_VALUE_EXP[0],x[0],x[len(x)-1],colors='royalblue',linestyle='dotted',label='PFM (Formvar)')
    plt.xticks(x, label_name, rotation=40, fontsize=12)
    plt.subplots_adjust(bottom=0.15, left=0.18, right=0.95, top=0.95)
    ax.set_ylabel('Force (nN)', fontsize=15)
    ax.set_xlim([x[0] - 1, x[len(x) - 1] + 1])
    ax.set_ylim([0.001, 100])
    ax.tick_params(axis="y", labelsize=14)
    ax.tick_params(axis="x", labelsize=14)
    ax.legend(fontsize=11, fancybox=True, loc="lower left")
    ax.yaxis.set_major_locator(MultipleLocator(5))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.yaxis.set_minor_locator(MultipleLocator(2.5))
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    ax.set_yscale("log")
    fig.savefig(filename)

def plot_forces_protrusive(forces_mb = None, filename = None, **kwargs):
    ##########################
    ## Plot data ############
    forces_01 = [x * 0.1 for x in forces_mb]
    plt.close()
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range(len(forces_mb)))]#Define x-axis values
    label_name = get_label_and_color_lists(len(forces_mb))[0]
    ##########################
    ## Plot the polymerization forces only: uncomment if needed #########
    plt.xticks(x, label_name, rotation=40, fontsize=12)
    ax.scatter(x, forces_mb,c='darkviolet',s=80,marker='o',label="Polymerization (10 pN)")
    ax.scatter(x, forces_01, c='cyan', s=80, marker='o',label="Polymerization (1 pN)")
    average_mb = np.average(forces_mb)
    average_01 = 0.1 * average_mb
    ax.hlines(average_mb,x[0],x[len(x)-1],colors='darkviolet',linestyle='dotted',label='Average value: %0.3f nN'%(average_mb), lw = 3)
    ax.hlines(average_01, x[0], x[len(x) - 1], colors='cyan', linestyle='dotted',
              label='Average value: %0.3f nN' % (average_01), lw=3)
    ax.set_ylim([0,1.8])
    ax.set_xlim([x[0]-1,x[len(x)-1]+1])
    ax.set_ylabel('$F_{polym}^{core}$(nN)',fontsize=17)
    ax.tick_params(axis='y', labelsize=14)
    ax.tick_params(axis="x", labelsize=14)
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    ax.yaxis.set_major_locator(MultipleLocator(0.3))
    ax.yaxis.set_minor_locator(MultipleLocator(0.15))
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    ax.legend(fontsize=11, fancybox=True, loc="upper right")
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    fig.savefig(filename)


def plot_pressure(pressure_el=None, pressure_mb=None, filename = None, **kwargs):
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax = plt.subplots()
    x = [k+1 for k in list(range(len(pressure_el)))]#Define x-axis values
    label_name = get_label_and_color_lists(len(pressure_el))[0]
    pressure_01 = [xx * 0.1 for xx in pressure_mb]

    ax.scatter(x,pressure_el,c='black',s=50, label='Elastic pressure',marker='o')
    ax.scatter(x,pressure_mb,c='orchid',s=60, label='Pol. pressure (10 pN)',marker='o')
    ax.scatter(x, pressure_01, c='cyan', s=60, label='Pol. pressure (1 pN)', marker='o')

    PRESSURE_VALUE_EXP = [80, 59]
    ax.hlines(PRESSURE_VALUE_EXP[0], x[0], x[len(x) - 1], colors='royalblue', linestyle='dotted', label='PFM (Formvar)')

    plt.xticks(x, label_name, rotation=40, fontsize=12)
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    ax.set_ylabel('Pressure (kPa)',fontsize=15)
    ax.tick_params(axis="y", labelsize=14)
    ax.tick_params(axis="x", labelsize=14)
    ax.set_ylim([0.01,1000])
    ax.set_xlim([x[0]-1,x[len(x)-1]+1])
    ax.yaxis.set_major_locator(MultipleLocator(40))
    ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.yaxis.set_minor_locator(MultipleLocator(20))
    ax.set_yscale("log")
    #ax.legend(fontsize=11, fancybox=True, loc="lower left")
    ax.tick_params(which='both', width=1.1)
    ax.tick_params(which='major', length=6)
    ax.tick_params(which='minor', length=4)
    fig.savefig(filename)


def plot_protrusive_filaments(orientation = None, orientation_std = None, number_mb=None, filename=None, **kwargs):
    ##########################
    ## Plot data #############
    plt.close()
    fig, ax1 = plt.subplots()
    n_podos = len(orientation)
    x = [k+1 for k in list(range(n_podos))]#Define x-axis values
    label_name = get_label_and_color_lists(n_podos)[0]
    ax1.hlines(np.mean(orientation), x[0], x[-1],colors='darkorange',linestyle='dotted',lw=3)
    ax1.errorbar(x,orientation, yerr=orientation_std, fmt='o', c='darkorange')
    ##########################
    ## Plot settings #########
    plt.xticks(x, label_name, rotation=40,fontsize=12)
    plt.yticks(fontsize=8)
    ax1.set_ylabel('Orientation of protrusive filaments ($°$)',color='darkorange',fontsize=15)
    ax1.set_xlim([x[0]-1,x[len(x)-1]+1])
    ax1.set_ylim([0,90])
    ax1.tick_params(axis='y', labelcolor='darkorange', labelsize=14)
    ax1.yaxis.set_major_locator(MultipleLocator(15))
    ax1.tick_params(which='both', width=1.1)
    ax1.tick_params(which='major', length=6)
    ax1.tick_params(which='minor', length=4)
    ax1.tick_params(axis="y", labelsize=14)
    ax1.tick_params(axis="x", labelsize=14)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.set_ylabel('Number of protrusive filaments', color='darkgreen',fontsize=15)  # we already handled the x-label with ax1
    ax2.scatter(x,number_mb,c='darkgreen',s=50,marker='s', alpha = 0.9)
    ax2.hlines(np.mean(number_mb),x[0],x[-1],colors='darkgreen',linestyle='dotted',lw=3)
    ax2.tick_params(axis='y', labelcolor='darkgreen', labelsize=14)
    ax2.set_ylim([0,140])#140 is optimal
    ax2.tick_params(which='both', width=1.1)
    ax2.tick_params(which='major', length=6)
    ax2.tick_params(which='minor', length=4)
    ax2.tick_params(axis="y", labelsize=14)
    plt.subplots_adjust(bottom=0.15,left=0.18,right=0.95,top=0.95)
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    fig.savefig(filename)
