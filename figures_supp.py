import src.analyze_article as aa
import src.podosome_analyzer as poda
import numpy as np
import src.plot_results as pc

def get_orientation_array(podosome, rmin=None, rmax=None):
    """ Array of orientation of filaments, taking the part strictly between rmin and rmax """
    segments = podosome["segments"]
    segs = segments[poda.in_radius(segments, rmin, rmax), :]
    ixes = np.unique(segs[:,0])
    fils = np.zeros(np.shape(ixes))
    for i,ix in enumerate(ixes):
        fils[i] = np.mean( poda.get_orientations(segs[segs[:, 0] == ix, :]))
    return fils


def get_length_array(podosome, rmin=None, rmax=None):
    """ Array of length of filaments strictly between rmin and rmax """
    if rmax is np.inf:
        def exclude(fil): return np.sum(poda.in_radius(fil, 0, rmin)) #excludes filaments inside
    else:
        def exclude(fil): return np.sum(poda.in_radius(fil, rmax, np.inf)) #excludes filaments outside
    nf = len(podosome["filaments"])
    lens = np.zeros((nf,))
    count = 0
    for i, filament in enumerate(podosome["filaments"]):
        if not exclude(filament):
            lens[count] = poda.fil_length(filament)
            count += 1
    return lens[0:count]

def get_single_array(list_of_arrays):
    """ Assembles a list of array into a single array """
    lens = [np.shape(arr)[0] for arr in list_of_arrays]
    single = np.zeros((np.sum(lens),))
    count = 0
    for i, arr in enumerate(list_of_arrays):
        single[count:count+lens[i]] = arr
        count += lens[i]
    return single

if __name__=="__main__":
    # Analyzing
    analyzed = aa.article_analyze(load_config="config_loader.yaml", analysis_config="config_analysis.yaml",
                                  characters="data_characteristics.csv")
    radii = analyzed.podosomes[0]["radial_results"]["radii"]

    # A trick if we have many podosomes
    if len(analyzed.podosomes)>5:
        index = 5
    else:
        index = 1

    # Plotting one specific podosome
    podosome= analyzed.podosomes[index]
    lengths = podosome["radial_results"]["fil_length"]
    fits_le = podosome["fit_length"]
    pc.plot_data_radius_individual(radius_centered=radii, values=lengths, data_fit=fits_le,
                                   filename="results/plots/length_podosome_6.pdf", tag="length", index=index)
    lengths = podosome["radial_results"]["length_density"]
    fits_le = podosome["fit_density"]
    pc.plot_data_radius_individual(radius_centered=radii, values=lengths, data_fit=fits_le,
                                   filename="results/plots/density_podosome_6.pdf", tag="filament_density", index=index)
    lengths = podosome["radial_results"]["energy_density"]
    fits_le = podosome["fit_energy"]
    pc.plot_data_radius_individual(radius_centered=radii, values=lengths, data_fit=fits_le,
                                   filename="results/plots/energy_podosome_6.pdf", tag="density_energy", index=index)
    lengths = podosome["radial_results"]["AVG_orientation"]
    fits_le = podosome["fit_orient"]
    pc.plot_data_radius_individual(radius_centered=radii, values=lengths, data_fit=fits_le,
                                   filename="results/plots/orient_podosome_6.pdf", tag="orientation", index=index)

    # For all podosomes
    radius_orient = [podosome["fit_orient"]["radius_core"] for podosome in analyzed.podosomes]
    radius_length = [podosome["fit_length"]["radius_core"] for podosome in analyzed.podosomes]
    radius_density = [podosome["fit_density"]["radius_core"] for podosome in analyzed.podosomes]
    radius_energy = [podosome["fit_energy"]["radius_core"] for podosome in analyzed.podosomes]

    pc.plot_core_radius_comparison_all(radius_orient=radius_orient, radius_length=radius_length,
                                       radius_density=radius_density, radius_energy=radius_energy,
                                       filename="results/plots/radius_comparison.pdf")

    pc.plot_core_radius_comparison_average_all(radius_orient=radius_orient, radius_length=radius_length,
                                       radius_density=radius_density, radius_energy=radius_energy,
                                       filename="results/plots/radius_comparison_average.pdf")

    fits = [podosome["fit_orient"] for podosome in analyzed.podosomes]
    core_limit = [fit["radius_core"] - np.abs(fit["parameter_c"]) for fit in fits]
    outr_limit = [fit["radius_core"] + np.abs(fit["parameter_c"]) for fit in fits]

    # Orientations
    orientations = [ get_orientation_array(podosome, 0, core_limit[i]) for i,podosome, in enumerate(analyzed.podosomes)]
    pc.plot_filament_orientation(orientations= orientations, filename="results/plots/orientations_core.pdf")
    orientations = get_single_array(orientations)
    pc.plot_filament_orientation_distribution(orientations=orientations,  color="red", capos = "upper left",
                                              filename="results/plots/core_orientation_distrib.pdf")
    print(f"Mean fil angle in : {np.mean(orientations)} +/- {np.std(orientations)}")

    orientations = [get_orientation_array(podosome, outr_limit[i], np.inf) for i, podosome, in enumerate(analyzed.podosomes)]
    pc.plot_filament_orientation(orientations=orientations, filename="results/plots/orientations_outr.pdf")
    orientations = get_single_array(orientations)
    pc.plot_filament_orientation_distribution(orientations=orientations, color="green", capos = "upper right",
                                              filename="results/plots/outr_orientation_distrib.pdf")
    print(f"Mean fil angle out : {np.mean(orientations)} +/- {np.std(orientations)}")

    # Lengths
    lengths = [get_length_array(podosome, 0, core_limit[i]) for i, podosome, in enumerate(analyzed.podosomes)]
    pc.plot_filament_length(lengths=lengths, filename="results/plots/length_core.pdf")
    lengths = get_single_array(lengths)
    pc.plot_filament_length_distribution(lengths=lengths, filename="results/plots/core_length_distrib.pdf",
                                         color="red", capos="upper left")
    print(f"Mean fil length in : {np.mean(lengths)} +/- {np.std(lengths)}")


    lengths = [get_length_array(podosome, outr_limit[i], np.inf) for i, podosome, in enumerate(analyzed.podosomes)]
    pc.plot_filament_length(lengths=lengths, filename="results/plots/length_outr.pdf")
    lengths = get_single_array(lengths)
    pc.plot_filament_length_distribution(lengths=lengths, filename="results/plots/outr_length_distrib.pdf",
                                         color="green", capos="upper right")
    print(f"Mean fil length out : {np.mean(lengths)} +/- {np.std(lengths)}")
