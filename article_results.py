import src.analyze_article as aa
import src.podosome_analyzer as poda
import src.plot_results as pc
import numpy as np
import pandas as pd

# Utilities
def get_orientation_array(podosome, rmin=None, rmax=None):
    """ Array of orientation of filaments, taking the part strictly between rmin and rmax """
    segments = podosome["segments"]
    segs = segments[poda.in_radius(segments, rmin, rmax), :]
    ixes = np.unique(segs[:,0])
    fils = np.zeros(np.shape(ixes))
    for i,ix in enumerate(ixes):
        fils[i] = np.mean( poda.get_orientations(segs[segs[:, 0] == ix, :]))
    return fils

def get_length_array(podosome, rmin=None, rmax=None):
    """ Array of length of filaments strictly between rmin and rmax """
    if rmax is np.inf:
        def exclude(fil): return np.sum(poda.in_radius(fil, 0, rmin)) #excludes filaments inside
    else:
        def exclude(fil): return np.sum(poda.in_radius(fil, rmax, np.inf)) #excludes filaments outside
    nf = len(podosome["filaments"])
    lens = np.zeros((nf,))
    count = 0
    for i, filament in enumerate(podosome["filaments"]):
        if not exclude(filament):
            lens[count] = poda.fil_length(filament)
            count += 1
    return lens[0:count]

def get_single_array(list_of_arrays):
    """ Assembles a list of array into a single array """
    lens = [np.shape(arr)[0] for arr in list_of_arrays]
    single = np.zeros((np.sum(lens),))
    count = 0
    for i, arr in enumerate(list_of_arrays):
        single[count:count+lens[i]] = arr
        count += lens[i]
    return single

# Main
if __name__=="__main__":
    analyzed = aa.article_analyze(load_config="config_loader.yaml", analysis_config="config_analysis.yaml",
                                  characters="data_characteristics.csv")
    radii = analyzed.podosomes[0]["radial_results"]["radii"]

    ## Saving
    # Saving radial results
    for podosome in analyzed.podosomes:
        df = pd.DataFrame.from_dict(podosome["radial_results"])
        df.to_csv("results/individual/radial_results_%s.csv" %podosome["podosome_name"])
    # Saving inner vs outer
    core = pd.DataFrame.from_records([podosome["core_results"] for podosome in analyzed.podosomes])
    outr = pd.DataFrame.from_records([podosome["outr_results"] for podosome in analyzed.podosomes])
    core.to_csv("results/general/core.csv")
    outr.to_csv("results/general/outr.csv")

    # ---------------------------------------------------------------------------------
    # Main text results :
    # ---------------------------------------------------------------------------------
    # Plotting orientation
    orientations = [ podosome["radial_results"]["AVG_orientation"] for podosome in analyzed.podosomes]
    numbers = [podosome["outr_results"]["number"] for podosome in analyzed.podosomes]
    fits_orient  = [ podosome["fit_orient"] for podosome in analyzed.podosomes]
    pc.plot_data_as_function_of_radius(radius_centered = radii, fits = fits_orient, results = orientations,
                                       filename = "results/plots/orientation.pdf", tag = "orientation")
    radiuses = [f["radius_core"] for f in fits_orient]
    print(f"Mean core radius : {np.mean(np.array(radiuses))} +/- {np.std(np.array(radiuses))}")
    pc.plot_core_radius(radiuses, "results/plots/insert_core_radius.pdf")


    # Plotting length
    lengths = [podosome["radial_results"]["fil_length"] for podosome in analyzed.podosomes]
    fits_le = [podosome["fit_length"] for podosome in analyzed.podosomes]
    pc.plot_data_as_function_of_radius(radius_centered=radii, fits=fits_le, results=lengths,
                                       filename="results/plots/length.pdf", tag="length")

    # Plotting density
    density = [podosome["radial_results"]["length_density"] for podosome in analyzed.podosomes]
    fits_de = [podosome["fit_density"] for podosome in analyzed.podosomes]
    pc.plot_data_as_function_of_radius(radius_centered=radii, fits=fits_de, results=density,
                                       filename="results/plots/length_density.pdf", tag="filament_density")

    # Plotting energy density
    energy = [podosome["radial_results"]["energy_density"] for podosome in analyzed.podosomes]
    fits_en = [podosome["fit_energy"] for podosome in analyzed.podosomes]
    pc.plot_data_as_function_of_radius(radius_centered=radii, fits=fits_en, results=energy,
                                       filename="results/plots/energy_density.pdf", tag="density_energy")

    # Plotting compression
    compression = [podosome["radial_results"]["strain"] for podosome in analyzed.podosomes]
    pc.plot_compression(radius_centered=radii, results=compression, filename="results/plots/new_compression.pdf")

    # Plotting energy per length
    e_per_length = [podosome["radial_results"]["energy_per_length"] for podosome in analyzed.podosomes]
    pc.plot_generic(radius_centered=radii, results=e_per_length, filename="results/plots/energy_length.pdf",
                    xlabel="Radius (nm)", ylabel='Energy / length ($k_B T/\mu m$)')

    number_mb = [podosome["core_results"]["number_mb"] for podosome in analyzed.podosomes]
    av_angle_mb = [podosome["core_results"]["AVG_angle_mb"] for podosome in analyzed.podosomes]
    st_orient_mb = [podosome["core_results"]["STD_angle_mb"] for podosome in analyzed.podosomes]
    pc.plot_protrusive_filaments(orientation=av_angle_mb, orientation_std=st_orient_mb,
                                 number_mb=number_mb, filename="results/plots/orient_number_mb.pdf")
    print(f"Mean number mb : {np.mean(number_mb)} ; std : {np.std(number_mb)}")
    print(f"Mean angle mb : {np.mean(av_angle_mb)} ; std : {np.std(av_angle_mb)}")

    # Forces
    forces_mb = [podosome["core_results"]["force_mb"]/1000 for podosome in analyzed.podosomes]
    forces_el = [podosome["core_results"]["elastic_force"] for podosome in analyzed.podosomes]
    pc.plot_forces(forces_mb = forces_mb, forces_el=forces_el, filename = "results/plots/forces_comp.pdf")
    pc.plot_forces_protrusive(forces_mb=forces_mb, filename="results/plots/forces_protrusive.pdf")
    print(f"Mean elastic force : {np.mean(forces_el)} ; std : {np.std(forces_el)}")
    print(f"--------------- min : {np.min(forces_el)} ; max : {np.max(forces_el)}")
    print(f"Mean protrusive force : {np.mean(forces_mb)} ; std : {np.std(forces_mb)}")


    # Pressure
    pressures_el = [podosome["core_results"]["pressure_el"]/1000 for podosome in analyzed.podosomes]
    pressures_mb = [podosome["core_results"]["pressure_mb"]/1000000 for podosome in analyzed.podosomes]
    pc.plot_pressure(pressure_mb=pressures_mb, pressure_el=pressures_el, filename="results/plots/pressure_comp.pdf")
    print(f"Mean elastic pressure : {np.mean(pressures_el)} ; std : {np.std(pressures_el)}")

    # Plotting correlation length
    corr_in = [podosome["core_results"]["corr_length"]/1000 for podosome in analyzed.podosomes]
    corr_out = [podosome["outr_results"]["corr_length"]/1000 for podosome in analyzed.podosomes]
    pc.plot_corr_in_out(values_in=corr_in, values_out=corr_out,
                        filename="results/plots/correlation_inout.pdf",xlabel=None,ylabel="Correlation length ($\mu m$)")
    pc.plot_in_out(values_in=corr_in, values_out=corr_out, filename="results/plots/correlation_bis.pdf", xlabel=None,
                        ylabel="Correlation length ($\mu m$)",ymin=0,ymax=3.5)
    print(f"Mean correlation length core : {np.mean(corr_in)} ; outside : {np.mean(corr_out)}")
    print(f"Std correlation length core : {np.std(corr_in)} ; outside : {np.std(corr_out)}")

    EPL_in = [podosome["core_results"]["energy_per_length"] for podosome in analyzed.podosomes]
    EPL_out = [podosome["outr_results"]["energy_per_length"] for podosome in analyzed.podosomes]
    pc.plot_in_out(values_in=EPL_in, values_out=EPL_out,
                   filename="results/plots/energy_per_length_inout.pdf", xlabel=None,
                   ylabel="Energy per length ($k_B T / \mu m$)")
    print(f"Mean energy per length core : {np.mean(EPL_in)} ; outside : {np.mean(EPL_out)}")

    energy_in = [podosome["core_results"]["energy_density"] for podosome in analyzed.podosomes]
    energy_out = [podosome["outr_results"]["energy_density"] for podosome in analyzed.podosomes]
    print(f"Mean energy per volume core : {np.mean(energy_in)} ; outside : {np.mean(energy_out)}")

    strain_in = [podosome["core_results"]["strain"] for podosome in analyzed.podosomes]
    strain_out = [podosome["outr_results"]["strain"] for podosome in analyzed.podosomes]
    print(f"Mean strain in : {np.mean(strain_in)} ; std : {np.std(strain_in)}")
    print(f"Mean strain out : {np.mean(strain_out)} ; std : {np.std(strain_out)}")

    young_in = [p/strain_in[i] for i,p in enumerate(pressures_el)]
    print(f"Mean Young modulus : {np.mean(young_in)} ; std : {np.std(young_in)}")

    # ---------------------------------------------------------------------------------
    # Supplementary :
    # ---------------------------------------------------------------------------------
    # For all podosomes
    radius_orient = [podosome["fit_orient"]["radius_core"] for podosome in analyzed.podosomes]
    radius_length = [podosome["fit_length"]["radius_core"] for podosome in analyzed.podosomes]
    radius_density = [podosome["fit_density"]["radius_core"] for podosome in analyzed.podosomes]
    radius_energy = [podosome["fit_energy"]["radius_core"] for podosome in analyzed.podosomes]

    pc.plot_core_radius_comparison_all(radius_orient=radius_orient, radius_length=radius_length,
                                       radius_density=radius_density, radius_energy=radius_energy,
                                       filename="results/plots/radius_comparison.pdf")

    pc.plot_core_radius_comparison_average_all(radius_orient=radius_orient, radius_length=radius_length,
                                       radius_density=radius_density, radius_energy=radius_energy,
                                       filename="results/plots/radius_comparison_average.pdf")

    fits = [podosome["fit_orient"] for podosome in analyzed.podosomes]
    core_limit = [fit["radius_core"] - np.abs(fit["parameter_c"]) for fit in fits]
    outr_limit = [fit["radius_core"] + np.abs(fit["parameter_c"]) for fit in fits]

    # Orientations
    orientations = [get_orientation_array(podosome, 0, core_limit[i])
                     for i,podosome, in enumerate(analyzed.podosomes)]
    pc.plot_filament_orientation(orientations= orientations, filename="results/plots/orientations_core.pdf")
    orientations = get_single_array(orientations)
    pc.plot_filament_orientation_distribution(orientations=orientations,  color="red", capos = "upper left",
                                              filename="results/plots/core_orientation_distrib.pdf")
    print(f"Mean fil angle in : {np.mean(orientations)} +/- {np.std(orientations)}")

    orientations = [get_orientation_array(podosome, outr_limit[i], np.inf)
                    for i, podosome, in enumerate(analyzed.podosomes)]
    pc.plot_filament_orientation(orientations=orientations, filename="results/plots/orientations_outr.pdf")
    orientations = get_single_array(orientations)
    pc.plot_filament_orientation_distribution(orientations=orientations, color="green", capos = "upper right",
                                              filename="results/plots/outr_orientation_distrib.pdf")
    print(f"Mean fil angle out : {np.mean(orientations)} +/- {np.std(orientations)}")

    # Lengths
    lengths = [get_length_array(podosome, 0, core_limit[i]) for i, podosome, in enumerate(analyzed.podosomes)]
    pc.plot_filament_length(lengths=lengths, filename="results/plots/length_core.pdf")
    lengths = get_single_array(lengths)
    pc.plot_filament_length_distribution(lengths=lengths, filename="results/plots/core_length_distrib.pdf",
                                         color="red", capos="upper left")
    print(f"Mean fil length in : {np.mean(lengths)} +/- {np.std(lengths)}")


    lengths = [get_length_array(podosome, outr_limit[i], np.inf) for i, podosome, in enumerate(analyzed.podosomes)]
    pc.plot_filament_length(lengths=lengths, filename="results/plots/length_outr.pdf")
    lengths = get_single_array(lengths)
    pc.plot_filament_length_distribution(lengths=lengths, filename="results/plots/outr_length_distrib.pdf",
                                         color="green", capos="upper right")
    print(f"Mean fil length out : {np.mean(lengths)} +/- {np.std(lengths)}")

    # A trick if we have many podosomes
    if len(analyzed.podosomes) > 5:
        index = 5
    else:
        index = 1

    # Plotting one specific podosome
    podosome = analyzed.podosomes[index]
    lengths = podosome["radial_results"]["fil_length"]
    fits_le = podosome["fit_length"]
    pc.plot_data_radius_individual(radius_centered=radii, values=lengths, data_fit=fits_le,
                                   filename="results/plots/length_podosome_6.pdf", tag="length", index=index)
    lengths = podosome["radial_results"]["length_density"]
    fits_le = podosome["fit_density"]
    pc.plot_data_radius_individual(radius_centered=radii, values=lengths, data_fit=fits_le,
                                   filename="results/plots/density_podosome_6.pdf", tag="filament_density", index=index)
    lengths = podosome["radial_results"]["energy_density"]
    fits_le = podosome["fit_energy"]
    pc.plot_data_radius_individual(radius_centered=radii, values=lengths, data_fit=fits_le,
                                   filename="results/plots/energy_podosome_6.pdf", tag="density_energy", index=index)
    lengths = podosome["radial_results"]["AVG_orientation"]
    fits_le = podosome["fit_orient"]
    pc.plot_data_radius_individual(radius_centered=radii, values=lengths, data_fit=fits_le,
                                   filename="results/plots/orient_podosome_6.pdf", tag="orientation", index=index)
